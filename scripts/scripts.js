$(document).ready(function(){
    $("#user_table").tablesorter();
    $("#leave_table").tablesorter();
    $("#recovery_table").tablesorter();
    $("#spr_table").tablesorter();
    $("table.tablesorter").tablesorter();

    $(".nav-item > a").click(function(){
        $("#administration").stop().animate({height: "toggle"}, 100)
        setTimeout(function(){
            $("#administration").stop().animate({height: "toggle"}, 200)
        }, 3000);
    })

    $(".select-all").click(function(){
        var table = $(this).parents("table");
        var allRadio = table.find("input[type='radio']");
        var select;
        if($(this).hasClass("yes-input"))
            select = table.find("input[value='1']");
        else if($(this).hasClass("no-input"))
            select = table.find("input[value='0']");
        else if($(this).hasClass("standby-input"))
            select = table.find("input[value='2']");
        for(i=0;i<allRadio.length;i++)
            allRadio[i].removeAttribute("checked");
        for(i=0;i<select.length;i++){
            select[i].setAttribute("checked", "checked");
            select[i].checked = true;
        }
    });

    Date.prototype.dateFormat = function() {
        var days   = ['Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi'];
        var months = ['Janvier','Fevrier','Mars','Avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Decembre'];
        var D    = this.getDay();
        var dd   = this.getDate().toString();
        var mm   = (this.getMonth()).toString();
        var yyyy = this.getFullYear().toString();
        return days[D]+" "+(dd[1]?dd:"0"+dd[0])+" "+months[mm]+" "+yyyy;
    };

    $(".date").each(function(){
        var date = new Date(this.innerText);
        $(this).html(date.dateFormat());
    });

    $("button[name^='event-']").click(function(){
        var id = this.name.split("-").pop();
        var event = getEventbyID(id);
        var txt = event.category+" du "+event.day;
        txt += "\nCampus : "+event.campus;
        txt += "\nDescription : "+event.description;
        alert(txt);
    });

    $(".show-description").each(function(){
        var description = this.innerText;
        $(this).html('<button type="button">Voir la description</button>');
        $(this).children("button").click(function(){
            alert("Description:\n"+description);
        });
    });
    $(".show-event").each(function(){
        var event = JSON.parse(this.innerText);
        $(this).html('<button type="button">Voir l\'évenement</button>');
        $(this).children("button").click(function(){
            var txt = event["id_category"]+" : "+event["day"]+"\n";
            txt += "Description : "+event["description"];
            alert(txt);
        });
    });
    $(".show-campus").each(function(){
      console.log(this);
        var id = this.innerText;
        $(this).html('<button type="button" name="eventCampus-'+id+'">Voir les campus concernés</button>');
        $(this).children("button").click(function(){
            alert("Liste des campus concernés:\n"+id);
        });
    });

    //leave/my
    $(".request-form").hide();
    $(".form-add a").click(function(){
        var formTarget = this.className;
        $("."+formTarget+".form").toggle("slow");
    });

    var start;
    var end;
    var startDay;
    var endDay;
    var date = new Date();
    var intervalHolidaysLength = 0;
    var intervalHolidays=[];
    var text;
    var new_solde= -1;
    var html;
    var DST = [];
    daylightSun_change();

    $("#leave-form input[type='reset']").click(function(){
        $("#leave-form").trigger("reset");
        $("#leave-form td > span#dt").html(0);
        $("#leave-form td > span#jo").html(0);
        $("#leave-form td > span#ns").html(solde);
        $("#leave-form td > span#limit").html("");
        $("img#invalid").css("display","none");
        $("#leave-form td > span#limit").html("");
        $("#leave-form td > #button_feries").attr("disabled", "false" );
        $("#leave-form input[type='submit']").attr( "disabled", "false" );
        intervalHolidaysLength = 0;
        intervalHolidays =[];
        start = "";
        end = "";
        text = "";
        new_solde = -1;
   });
    $('#leave-form input[name="start"]').change(function(){
        start = new Date(format_date_datepicker(this.value));
        if (start>date){
            start.setHours(0,0,0,0);
            startDay = start.getDay();
        }
    });
    $('#leave-form input[name="end"]').change(function(){
        end = new Date(format_date_datepicker(this.value));
        end.setHours(0,0,0,0);
        endDay = end.getDay();
    });
    $('#leave-form input[type="text"]').change(function(){
        if (start!=" " && end!=" "){
            if(start<=end && date<end && date<start){
                var days= Math.floor((end.getTime()-start.getTime())/86400000);
                var day = start.getTime();
                var jo=1;
                for(i=0;i<days;i++){
                    var dateDay = new Date(day+= 86400000);
                    if(DST[0]<dateDay && dateDay<=DST[1]){
                        dateDay.setHours(dateDay.getHours()+1,0,0,0);
                    }
                    if(dateDay.getDay()!= 6 && dateDay.getDay()!= 0){
                        jo++;
                    }
                }
                var intervalHoliday = {};
                intervalHolidays = [];
                var intevalDate = new Date(start);
                for(intevalDate;intevalDate<=end.getTime();intevalDate.setDate(intevalDate.getDate() + 1)){
                   for (var i=0;i<holidays.length;i++){
                        if(holidays[i]["timestamp"]*1000 == intevalDate.getTime()){
                            intervalHoliday = {
                                "name" : holidays[i]["name"],
                                "date" : holidays[i]["timestamp"]*1000
                            }
                            var incrementHoli = new Date(holidays[i]["timestamp"]*1000);
                            if(incrementHoli.getUTCDay()!== 6 && incrementHoli.getUTCDay()!== 0){
                                intervalHolidaysLength ++;
                            }; intervalHolidays.push(intervalHoliday);
                        }
                    }
                };
                var jo2 = (jo - intervalHolidaysLength);
                days = days+1;
                new_solde = solde-jo2;
                text = "Liste des jours fériés :\n";
                for(var i=0;i<intervalHolidays.length;i++){
                    date_holiday = new Date(intervalHolidays[i]["date"]);
                    var monthNames = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"];
                    intervalHolidays[i]["date"] = ('0'+date_holiday.getDate()).slice(-2)+" "+ monthNames[date_holiday.getMonth()]+" "+date_holiday.getFullYear();
                    text += intervalHolidays[i]["date"]+" : "+intervalHolidays[i]["name"]+",\n";
                }
                text = text.slice(0,-2)+".";

                if (intervalHolidays.length>0){ $("#button_feries").removeAttr("disabled");}
                $("#leave-form td > span#dt").html(days);
                $("#leave-form td > span#jo").html(jo2);
                html = "<span>"+solde+"</span>";
                html += "<span> - </span>";
                html += "<span>"+jo2+"</span>";
                html += "<span> = </span>";
                html += "<span>"+new_solde+"</span>";

                $("#leave-form tbody td:last-child ").html(html);
                if (new_solde>=0){
                $(this).parents("table").find("input[type='submit']").removeAttr( "disabled" );
                $("#leave-form td > span#limit").html("");
                $("img#invalid").css("display","none");
                }else{
                    $(this).parents("table").find("input[type='submit']").attr( "disabled", "false" );
                    $("#leave-form td > span#limit").html("Dépassement du solde !");
                    $("img#invalid").css("display","inline-block");}
            } else {
                $(this).parents("table").find("input[type='submit']").attr( "disabled", "false" );
                $("#button_feries").attr("disabled", "false" );
            }
        }
    });
    $('#button_feries').click(function(){
        alert(text);
    });

    if($('#recovery-form input[name="day"]').value)
            $('#recovery-form input[name="day"]').parents("table").find("input[type='submit']").removeAttr( "disabled" );
    else $('#recovery-form input[name="day"]').parents("table").find("input[type='submit']").attr( "disabled","false" );

    $("#recovery-form input[type='reset']").click(function(){
        $("#recovery-form td > span#recup_valide").html(" ");
        $("img#invalid").css("display","none");
        $("input[type='submit']").attr( "disabled","false");
    });
    $('#recovery-form input[name="day"]').change(function(){
        $("#recovery-form td > span#recup_valide").html("");
        var button = 0
        var start_r = new Date($(this).datepicker( "getDate" ));
        start_r.setHours(0,0,0,0);
        if(start_r > date){
            if(DST[0]<start_r && start_r<=DST[1]){
                start_r.setHours(start_r.getHours()+1,0,0,0);
            } else{
                start_r.setHours(start_r.getHours()+2,0,0,0);
            }
            if(start_r.getUTCDay()!==6 && start_r.getUTCDay()!==0){
                if(is_holiday(start_r)){
                    $("#recovery-form td > span#recup_valide").html("Jour fériés !");
                    $("img#invalid").css("display","inline-block");
                } else button = 1;
            } else {
                $("#recovery-form td > span#recup_valide").html("Week-end !");
                $("img#invalid").css("display","inline-block");
            }
            if(button == 1){
                $('#recovery-form input[name="day"]').parents("table").find("input[type='submit']").removeAttr( "disabled" );
                $("img#invalid").css("display","none");
            } else $('#recovery-form input[name="day"]').parents("table").find("input[type='submit']").attr( "disabled","false" );

        } else {
            $('#recovery-form input[name="day"]').parents("table").find("input[type='submit']").attr( "disabled","false" );
            $("#recovery-form td > span#recup_valide").html("Date passée!");
            $("img#invalid").css("display","inline-block");
        }
    });
    
    
    $('#leave-form input[name="unpaid_leave"]').change(function(){
        if(this.checked == true){
             var unpaid_leave = solde;
            $("#leave-form tbody td:last-child ").html(unpaid_leave);
        }else $("#leave-form tbody td:last-child ").html(html);
    });

// leave/my (subscribe)
    var eventSelect = $("#event-suscribe-form select[name='id_event']").html()
    $("#event-suscribe-form select[name='category']").change(function(){
        $category = $(this).val();
        if($(this).val()!=""){
            var html;
            if(enable_Event_Category[$category].length < 1) html += '<option value="">Aucun évènement disponible.</option>';
            else {
                html += '<option value="" selected>Selectionnez un évènement:</option>'
                for(var i=0;i<enable_Event_Category[$category].length;i++){
                    html += "<option value="+enable_Event_Category[$category][i]["id"]+">"+enable_Event_Category[$category][i]["description"]+" "+enable_Event_Category[$category][i]["day"]+"</option>";
                }
            } $("#event-suscribe-form select[name='id_event']").html(html);
        } else $("#event-suscribe-form select[name='id_event']").html(eventSelect);
    });

    $(".delete").click(function(){
        var button = this;
        var txt;
        var url;
        var catForm = {leave: myLeaves, recovery: myRecovery, inscription: myAllEvent};
        $.each(catForm, function(key, value){
            if($(button).parents("table").hasClass(key)){
                $.each(value,function(){
                    if(this.id == button.id){
                        switch(key){
                            case 'leave':
                                txt = "Vous etes sur le point d'annuler votre demande de congé du: "+this.start+" au "+this.end;
                                url = "../ajax/deleteMyLeave?id=";
                                break;
                            case "recovery":
                                txt = "Vous etes sur le point d'annuler votre demande de récuperation du: "+this.day;
                                url = "../ajax/deleteMyRecovery?id=";
                                break;
                            case "inscription":
                                txt = "Vous etes sur le point d'annuler votre inscription a lévenement: "+this.id;
                                url = "../ajax/deleteMySubscribe?id=";
                                break;
                        } txt += "\nContinuer ?";
                        var cond = confirm(txt);
                        if(cond) document.location.href = url+button.id;
                    }
                })
            }
        });
    });

//leave/my(datepicker)
    $(".datepicker").datepicker({
        dateFormat:'dd/mm/yy',
        showOtherMonths: true,
        selectOtherMonths: true,
        minDate: "+1d",
        maxDate: "+1Y",
        prevText: " ",
        nextText: " ",
        showAnim: "fold"});

//leave/my(functions)
    function is_holiday(ObjDate){
        var i;
        var is_holiday = false;
        ObjDate.setHours(0,0,0,0);
        var date = ObjDate.getTime();
        for(i=0;i<holidays.length;i++){
            if (date ==holidays[i]["timestamp"]*1000) is_holiday = true;
        } return is_holiday;
    }

    function format_date_datepicker($datepicker){
        var date_nombre = $datepicker.split('/');
        var date = date_nombre[2]+"-"+date_nombre[1]+"-"+date_nombre[0];
        return date;
    }
    function daylightSun_change(){
        var today=new Date();
        var actualYear=today.getFullYear();
        var lastSundaymarch;
        var lastSundayOct;
        for (var i=1;i <=2;i++){
            var month;
            if (i==1) month=2 ;
            else month=9;
            var days=31;
            var year=actualYear;
            do{
                var lastSunday=new Date( year, month, days,0,0,0,0);
                var nextYear=false;
                while( lastSunday.getDay()!==0)
                    {
                    days--;
                    lastSunday=new Date( year, month, days,0,0,0,0);
                    }
                if (today>lastSunday){
                    nextYear=true;
                    year++;}
                else nextYear=false;
            } while (nextYear);
            if (i==1)lastSundaymarch=lastSunday;
            else lastSundayOct=lastSunday;
        }
        DST = [lastSundayOct,lastSundaymarch];
        return DST;
    }

    // leave/calendar

  if(typeof currentPage !== 'undefined'){
    var CampusFC;
    var table_check = [];
    var tableau = [];
    for(var i=0;i<$(".check input[type='checkbox']").length;i++){
        table_check[i] = $(".check input[type='checkbox']")[i].checked;
    }
    for(var i=0;i<campusLeaves.length;i++){
        if(table_check[i] == true && campusLeaves[i].length>0){
            var leave_campus={
            "events": campusLeaves[i],
            "backgroundColor":"#0085B2",
            "borderColor": "#0085B2",
            "textColor":'black'}
            tableau.push(leave_campus);
        }else {
            var leave_campus= [];
            tableau.push(leave_campus);
        }
        CampusFC = tableau;
    }
    $('#calendar').fullCalendar({
        dayClick: function(){},
        header: {
            left: 'prev,next prevYear,nextYear today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        eventSources: CampusFC,
    });
    $(".check input[type='checkbox']").change(function () {

        if(CampusFC.length!==0){
            $('#calendar').fullCalendar('removeEvent');
            $('#calendar').fullCalendar('refetchEvents');
        }
        var table_check = [];
        for(var i=0;i<$(".check input[type='checkbox']").length;i++){
            table_check[i] = $(".check input[type='checkbox']")[i].checked;
        }
        for(var i=0;i<campusLeaves.length;i++){
        }
            var id = this.name -1;
            if(this.checked == true){
                $('#calendar').fullCalendar('addEventSource', tableau[id]);
                $('#calendar').fullCalendar('refetchEvents');
            } else {
                $('#calendar').fullCalendar('removeEventSource', tableau[id]);
                $('#calendar').fullCalendar('refetchEvents');
            }
    });
    $('#calendar').fullCalendar('option', 'aspectRatio', 1.55);

  }

    //user/getAll
    $("body").on('click', 'div.campus > a', function() {
        var divCampus = $(this).parent("div.campus");
        if($(this).hasClass("add")){
            var newBlock = '<div class="campus">'+divCampus.html()+'</div>';
            divCampus.after(newBlock);
            divCampus.children("a.add").addClass("delete");
            divCampus.children("a.add").removeClass("add");
        } else if($(this).hasClass("delete")) divCampus.remove();
    })

    $("table.leave-table").each(function(){
      var test = $(this).children("tbody").children("tr");
    })


});
