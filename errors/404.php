<?php ob_start();
//require_once '../includes/functions.php';
$title = "ERROR: 404";
define("ROOT", realpath(__DIR__ . "/../") . "/");
?>

<h1>La page que vous demandez n'est pas disponible</h1>

<?php
$content = ob_get_clean();
require_once '../template/template.php';
?>
