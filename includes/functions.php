<?php

/* Require Fonctions */
require_once 'functions_pdo.php';
/*** End Require Fonctions ***/

/* Constantes */
define("SCRIPT_FOLDER", "/sup_conges/scripts/");
define("STYLE_FOLDER", "/sup_conges/styles/");
define("LINK_FOLDER", "/sup_conges/");
/*** Fin Constantes ***/

/* Global Variables */
$styles = array();
$sripts = array();
/*** End Global Variables ***/

function link_Converter($ctrl, $view) {
    return LINK_FOLDER . $ctrl . "/" . $view;
}
function addStyle($style) {
    global $styles;
    $styles[] = $style;
}

function addScript($script) {
    global $scripts;
    $scripts[] = $script;
}

function stylesheets() {
    global $styles;
    $str = "";
    foreach ($styles as $style) {
        $str .= '<link rel="stylesheet" href="' . STYLE_FOLDER . $style . '.css" type="text/css" media="all" />' . "\n\t";
    }
    if (!empty($style)) echo rtrim($str, "\t");
}

function javascript() {
    global $scripts;
    $str = "";
    foreach ($scripts as $script) {
        $str .= '<script type="text/javascript" src="' . SCRIPT_FOLDER . $script . '.js"></script>' . "\n\t";
    }
    if (!empty($script)) echo rtrim($str, "\t");
}

function fullname($id_booster) {
    if(!is_null($id_booster) && $id_booster !="0"){
        $user = User::getOne($id_booster);
        return '<a href="../user/profil?id=' . $user->id_booster . '" target="_blank">' . $user->firstname . " " . $user->lastname . '</a>';
    } else return "undefined";
}

function dateFormat ($datepicker){
    var_dump($datepicker);
    $timestamp = strtotime(str_replace('/', '-', $datepicker));
    $date = date("Y-m-d",$timestamp);
    var_dump($date);
    return $date;
}
?>