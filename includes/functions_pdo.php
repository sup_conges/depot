<?php

define("BDD_HOST", "localhost");
define("BDD_LOGIN", "root");
define("BDD_PASSWORD", "");
define("BDD_NAME", "sup_conges");

$pdo = null;

function getPdo() {
    global $pdo;
    if ($pdo == null) {
        $pdo = new PDO("mysql:dbname=" . BDD_NAME . ";host=" . BDD_HOST, BDD_LOGIN, BDD_PASSWORD);
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    return $pdo;
}

function show_tables() {
    $table = getPdo()->query("show tables")->fetchAll();
    return $table;
}

function show_columns_from($table) {
    return getPdo()->query("describe " . TABLE_PREFIX . $table . ";")->fetchAll();
}

function insertinto_BDD($table, $values) {
    $TP_table = TABLE_PREFIX . $table;
    $insert = "INSERT INTO `" . $TP_table;
    if (!empty($values)) {
        $insert .= "` ( ";
        foreach ($values as $key => $value) {
            $insert .= "`" . $key . "`, ";
        }
        $insert = rtrim($insert, ", ") . " ) VALUES ( ";
        foreach ($values as $key => $post_value) {
            if ($key == "pagecontent") {
                $post_value = htmlspecialchars($post_value);
            }
            $insert .= '"' . $post_value . '", ';
        }
        $insert = rtrim($insert, ", ");
        $insert .= ");";
        getPdo()->exec($insert);
        return getPdo()->lastInsertId();
    }
    return false;
}

function kill_inputSubmit($array) {
    foreach ($array as $key => $value) {
        if ($value == "Envoyer") {
            array_pop($array);
        }
    }
    return $array;
}

function add_in_database($table, $values) {
    $TP_table = TABLE_PREFIX . $table;
    $query = "INSERT INTO " . $TP_table . " (";
    foreach ($values as $column => $value) {
        $query .= "`" . $column . "`, ";
    }
    $query = rtrim($query, ", ") . ") VALUES (";
    foreach ($values as $column => $value) {
        $query .= "'" . $value . "', ";
    }
    $query = rtrim($query, ", ") . ")";
    getPdo()->exec($query);
}
function add_in_BDD($table, $values) {
    $TP_table = $table;
    $query = "INSERT INTO " . $TP_table . " (";
    foreach ($values as $column => $value) {
        $query .= "`" . $column . "`, ";
    }
    $query = rtrim($query, ", ") . ") VALUES (";
    foreach ($values as $column => $value) {
        $query .= "'" . $value . "', ";
    }
    $query = rtrim($query, ", ") . ")";
    getPdo()->exec($query);
}

function addLine_in_table() {
    if (!empty($_POST)) {
        $input = "";
        foreach (array_keys($_POST) as $field) {
            if (strpos($field, "addin" . TABLE_PREFIX) === 0) {
                $input = $field;
                break;
            }
        }
        unset($_POST[$input]);
        insertinto_BDD(str_replace("addin" . TABLE_PREFIX, "", $input), $_POST);
    }
}

function get_page_title($pagename) {
    $ret = get_from_database("pages", "pagename", array("filename" => $pagename));
    if ($ret && !empty($ret)) {
        return $ret[0]["pagename"];
    } else {
        return "Le nom de la page est introuvable. :(";
    }
}

function get_from_database($table, $columns = "*", $cond = array(), $order = false, $fetch = PDO::FETCH_ASSOC) {
    $TP_table = TABLE_PREFIX . $table;
    $query = "SELECT " . $columns . " FROM `" . $TP_table . "`";
    if ($cond && !empty($cond)) {
        $count = 0;
        foreach ($cond as $col => $val) {
            $query .= ($count == 0 ? " WHERE `" : " AND `") . $col . "` = '" . $val . "'";
        }
    }
    if ($order)
        $query .= " ORDER BY `" . $order . "` ASC";
    $query .= ";";
    return getPdo()->query($query)->fetchAll($fetch);
}

function update_from_database($table, $cond = array(), $wherecolumn, $wherevalue) {
    $TP_table = TABLE_PREFIX . $table;
    if (count($cond) != 0) {
        $query = "UPDATE " . $TP_table . " SET ";
        foreach ($cond as $col => $val) {
            $query .= $col . "='" . htmlentities($val, ENT_QUOTES) . "', ";
        }
        $query = rtrim($query, ", ");
        $query .= " WHERE " . $wherecolumn . "=" . $wherevalue . ";";
        getPdo()->exec($query);
    }
}

function delete_from_BDD($table, $cond = array()) {
    $TP_table = TABLE_PREFIX . $table;
    if (!empty($cond)) {
        $query = "DELETE FROM `" . $TP_table . "`";
        $count = 0;
        foreach ($cond as $col => $val):
            $query .= " WHERE `" . $col . "` = '" . $val . "'";
        endforeach;
        $query .= ";";
        getPdo()->exec($query);
    }
}

function deletPagesOnBDD($table = array()) {
    foreach ($table as $POSTline):
        delete_from_BDD("pages", array("id" => $POSTline));
    endforeach;
}

function echo_pagecontent($page_id) {
    foreach (get_from_database("pages ", "id ", "$page_id") as $line):
        echo htmlspecialchars_decode($line["pagecontent"]);
    endforeach;
}

?>
<?php


function join_table($column) {
    $query = getPdo()->query(
                    "SELECT * FROM " . TABLE_PREFIX . "pages
            LEFT JOIN `" . TABLE_PREFIX . "categories` 
            ON `" . TABLE_PREFIX . "pages`.`id_categorie` 
                = `" . TABLE_PREFIX . "categories`.`id`
                    ORDER BY `id_categorie` ASC"
            )->fetchAll(PDO::FETCH_ASSOC);
    return $query;
}

?>