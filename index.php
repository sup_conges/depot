<?php
require_once 'config.php';

session_start();

require_once 'includes/functions.php';

require_once("models/Model.php");
require_once("models/User.php");
require_once("models/Leave.php");
require_once("models/Holiday.php");
require_once("models/Campus.php");
require_once("models/Region.php");
require_once("models/SPR_Event.php");

require_once("controllers/DefaultController.php");
require_once("controllers/AjaxController.php");
require_once("controllers/UserController.php");
require_once("controllers/LeaveController.php");


if (!isset($_GET["ctrl"])) $ctrlName = "default";
else $ctrlName = $_GET["ctrl"];

if (!isset($_GET["view"])) $viewName = "index";
else $viewName = $_GET["view"];

if(!isset($_SESSION['id_booster'])){
    if($_GET["ctrl"]!="user" && ($_GET["view"]!="login" || $_GET["view"]!="lostpassword"))
        header("Location: ".ROOT_URL."user/login");
} else {
    if(!isset($_SESSION['fullname'])){
        if($_GET["ctrl"]!="user" || $_GET["view"]!="update_self")
            header("Location: ".ROOT_URL."user/update_self");
    }
}

switch ($ctrlName) {
    case "default":
        $ctrl = new DefaultController();
        break;;
    case "ajax":
        $ctrl = new AjaxController();
        break;
    case "user":
        $ctrl = new UserController();
        break;
    case "leave":
        $ctrl = new LeaveController();
        break;
    case "error":
        $ctrl = new ErrorController();
        break;
    default:
        exit("Ce controleur n'existe pas");
}

if (!method_exists($ctrl, $viewName)) {
    exit("Cette vue n'existe pas");
}
$ctrl->$viewName();
?>
