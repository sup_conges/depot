<?php ob_start();
require_once 'includes/functions.php';
$title = "Mise a jours de mon profil";
?>

<h1><?= $title; ?></h1><hr/>
<div id="edit_user_form" class="form">
    <form action="?ctrl=ajax&view=update_self_user" method="POST">
        <fieldset>
            <legend><h2>Edition du compte: <?= $user->id_booster; ?></h2></legend>
            <label for="firstname">Prénom:</label>
            <input type="text" id="firstname" name="firstname" 
                   value="<?= $user->firstname; ?>" required/><br/>
            <label for="lastname">Nom:</label>
            <input type="text" id="lastname" name="lastname" 
                   value="<?= $user->lastname; ?>" required/><br/>
            <input type="hidden" id="id_booster" name="id_booster" 
                   value="<?= $user->id_booster; ?>" required/>
            <label for="password">Ancien mot de passe:</label>
            <input type="password" id="password" name="password" 
                   value="" required/><br/>
            <label for="password1">Nouveau mot de passe:</label>
            <input type="password" id="password1" name="password1" 
                   value="" required/><br/>
            <label for="password2">Retaper votre mot de passe:</label>
            <input type="password" id="password2" name="password2" 
                   value="" required/><br/>
            <input type="submit" value="Mettre a jour"/>
            <input type="reset" value="Reinitialiser"/>
        </fieldset>
    </form>
</div>

<script>
    var password1 = document.getElementById('password1');
    var password2 = document.getElementById('password2');
    
     var check_password_validity = function(){
        if(password1.value !== password2.value){
            password2.setCustomValidity('Les mots de passe ne correspondent pas!');
            password2.focus();
            password2.select();
        } 
        else {
        password2.setCustomValidity('');
         }
     };

  password1.addEventListener('change',check_password_validity,false);
  password2.addEventListener('change',check_password_validity,false);
</script>

<?php $content = ob_get_clean();
require_once 'template/template.php';
?>
