<?php ob_start();
require_once 'includes/functions.php';
$title = "Profil: ".$user->id_booster;
?>

<h1><?= $title; ?></h1><hr/>

<h2><?= $user->firstname." ".$user->lastname; ?></h2>
<table>
    <tr>
        <th>Nom complet</th>
        <td><?= $user->firstname." ".$user->lastname; ?></td>
    </tr>
    <tr>
        <th>ID Booster</th>
        <td><?= $user->id_booster; ?></td>
    </tr>
    <tr>
        <th>Email</th>
        <td><?= $user->email; ?></td>
    </tr>
    <tr>
        <th>Poste</th>
        <td><?= $user->job ?></td>
    </tr>
    <tr>
        <th>Campus</th>
        <td><?= $user->campus;?></td>
    </tr>
    <?php if($user->id_manager != NULL){ ?>
    <tr>
        <th>ID Manager</th>
        <td><?= $manager->getLink(); ?></td>
    </tr>
    <?php } ?>
</table>

<?php
$content = ob_get_clean();
require_once 'template/template.php';
?>
