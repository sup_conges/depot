<?php ob_start();
require_once 'includes/functions.php';
$title = "Administration des utilisateurs";
?>

<h1><?php echo $title; ?></h1><hr/>
<?php if($me->id_job>=3){ ?>
<div id="add_user_form" class="form">
    <form action="?ctrl=ajax&view=add_user" method="POST">
        <fieldset>
            <legend><h2>Ajouter un utilisateur</h2></legend>
            <label for="id_booster">ID Booster:</label>
            <input type="text" id="id_booster" name="id_booster" required/><br/>
            <label for="id_job">Poste:</label>
            <select name="id_job">
            <?php foreach ($jobs as $element){ ?>
                <option value="<?= $element->id ?>"><?= $element->job ?></option>
            <?php } ?>
            </select><br/>
            <label for="campus">Campus:</label>
            <div class="campus">
                <select id="campus" name="campus[]" required>
                    <option value="" selected>Selectionner un campus</option>
                <?php foreach ($campus as $element){ ?>
                    <option value="<?= $element->id ?>"><?= $element->name ?></option>
                <?php } ?>
                </select>
                <a class="add"></a>
            </div>
            <label for="id_manager">ID Manager:</label>
            <input type="text" id="id_manager" name="id_manager" required/><br/>
            <input type="submit" value="Ajouter un utilisateur"/>
            <input type="reset" value="Reinitialiser"/>
        </fieldset>
    </form>
</div>
<?php } ?>

<fieldset>
    <legend><h2> Liste des utilisateurs </h2></legend>
    <table class="user-table tablesorter">
        <thead>
            <tr>
                <th>Nom complet</th>
                <th>ID Booster</th>
                <th>Poste</th>
                <th>Manager</th>
                <th>Campus</th>
                <th>Email</th>
                <th>Congés restants</th>
                <th>Récupérations restantes</th>
                <th colspan="3">Actions</th>
            </tr>
        </thead>
        <tbody>
        <?php $i=0; foreach ($usersView as $userView) { ?>
            <tr>
                <td><a href="../user/profil?id=<?= $userView["id_booster"]; ?>" target="_blank"><?= $userView["fullname"]; ?></a></td>
                <td><?= $userView["id_booster"]; ?></td>
                <td><?= $userView["job"]; ?></td>
                <td><a href="../user/profil?id=<?= $userView["manager"]["id_booster"]; ?>" target="_blank"><?= $userView["manager"]["fullname"]; ?></a></td>
                <td><?= $userView["campus"]; ?></td>
                <td><?= $userView["email"]; ?></td>
                <td><?= $userView["leaveCount"]; ?></td>
                <td><?= $userView["RecoveryCount"]; ?></td>
                <td>
                    <a href="../user/profil&id=<?= $userView["id_booster"]; ?>" style="color: #464646;">Afficher</a>
                </td>
                <td>
                    <a href="../user/update?id=<?= $userView["id_booster"]; ?>" style="color: #464646;">Modifier</a>
                </td>
                <td>
                    <a href="../ajax/delete_user?id=<?= $userView["id_booster"]; ?>"
                       onclick="return(confirm('Etes-vous sur de vouloir supprimer lutilisateur: \n\
                       <?= "&quot;".$userView["id_booster"]." - ".$userView["fullname"]."&quot;" ?>'));"
                       style="color: red;">Supprimer</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</fieldset>

<?php $content = ob_get_clean();
require_once 'template/template.php';
?>
