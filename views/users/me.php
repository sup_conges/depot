<?php ob_start();
require_once 'includes/functions.php';
$title = "Mon compte";
?>

<h1><?= $title; ?></h1><hr/>
<div class="information">
    <table id="me">
        <tr>
            <th>Nom complet</th>
            <td><?= $me->firstname." ".$me->lastname; ?></td>
        </tr>
        <tr>
            <th>ID Booster</th>
            <td><?= $me->id_booster; ?></td>
        </tr>
        <tr>
            <th>Email</th>
            <td><?= $me->email; ?></td>
        </tr>
        <tr>
            <th>Poste</th>
            <td><?= $me->job; ?></td>
        </tr>
        <tr>
            <th>Campus</th>
            <td><?= $me->campus; ?></td>
        </tr>
        <?php if($me->id_manager != "0"){ ?>
        <tr>
            <th>Manager</th>
            <td><?= $me->id_manager; ?></td>
        </tr>
        <?php } ?>
        <tr>
            <th>Congés restants</th>
            <td><?= $me->getSolde(); ?></td>
        </tr>
        <tr>
            <th>Recupérations restantes</th>
            <td><?= $me->recovery_day; ?></td>
        </tr>
    </table>
    <div id="modify">
        <a href="<?= ROOT_URL ?>user/update_self">Modifier mes informations de compte</a>
    </div>
</div>

<?php $content = ob_get_clean();
require_once 'template/template.php';
?>
