<?php ob_start();
$title = "Edition du compte";
?>

<h1><?= $title ?></h1><hr/>
<div id="edit_user_form" class="form">
    <form action="?ctrl=ajax&view=update_user&id=<?= $user->id_booster; ?>" method="POST">
        <fieldset>
            <legend><h2>Compte: <?= $user->id_booster; ?></h2></legend>
            <label for="firstname">Prénom:</label>
            <input type="text" id="firstname" name="firstname"
                   value="<?= $user->firstname; ?>" required/><br/>
            <label for="lastname">Nom:</label>
            <input type="text" id="lastname" name="lastname"
                   value="<?= $user->lastname; ?>" required/><br/>
            <input type="hidden" id="id_booster" name="id_booster"
                   value="<?= $user->id_booster; ?>" required/>
            <label for="id_job">Poste</label>
            <select name="id_job">
            <?php foreach ($a_jobs as $job){ ?>
                <?php if($user->id_job == $element->id){ ?>
                <option value="<?= $job->id ?>" selected><?= $job->job ?></option>
                <?php } else { ?>
                <option value="<?= $job->id ?>"><?= $job->job ?></option>
                <?php } ?>
            <?php } ?>
            </select></br>
            <div class="campus">
                <select id="campus" name="campus[]" required>
                    <option value="" selected>Selectionner un campus</option>
                <?php foreach ($a_campus as $campus){ ?>
                    <option value="<?= $campus->id ?>"><?= $campus->name ?></option>
                <?php } ?>
                </select>
                <a class="add"></a>
            </div>
            <label for="id_manager">ID Manager:</label>
            <input type="text" id="id_manager" name="id_manager"
                   value="<?= $user->id_manager; ?>" required/><br/>
            <input type="submit" value="Mettre a jour"/>
            <input type="reset" value="Reinitialiser"/>
        </fieldset>
    </form>
</div>

<?php $content = ob_get_clean();
require_once 'template/template.php';
?>
