<?php ob_start();
require_once 'includes/functions.php';
$title = "forgot-password";
?>

<div id="log-window">
    <h1><?= $title ?></h1>
    <form action="<?= link_Converter("ajax", "reset_Passord"); ?>" method="post">
        <div id="user-input">
            <label for="id_booster">Adresse mail: </label>
            <input type="text" name="email" value="" required>
        </div>
        <hr/>
        <input type="submit" value="Envoyer">
        <a href="<?= link_Converter("user", "login"); ?>">Retourner vers la page de connexion</a>
    </form>
</div>

<?php $content = ob_get_clean();
require_once 'template/template.php';
?>