<?php ob_start();
require_once 'includes/functions.php';
$title = "Connexion";
?>

<div id="log-window">
    <h1><?= $title ?></h1>
    <form action="<?= link_Converter("ajax", "authentication"); ?>" method="post">
        <div id="user-input">
            <label for="id_booster">ID Booster: </label>
            <input type="text" name="id_booster" id="id_booster" value="" required>
        </div>
        <div id="passwordInput">
            <label for="password">Password: </label>
            <input type="password" name="password" id="password" value="">
        </div>
        <hr/>
        <input type="submit" value="Connexion">
        <a href="<?= link_Converter("user", "lostpassword"); ?>">J'ai oublie mon mot de passe</a>
    </form>
</div>

<?php $content = ob_get_clean();
require_once 'template/template.php';
?>
