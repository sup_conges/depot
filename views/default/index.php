<?php ob_start();
require_once 'includes/functions.php';
?>

<h1>Default controller</h1>
<p> Coucou c'est l'index </p>
<ul>
<?php 
// La variable myList est définie dans la fonction DefaultController->index
foreach($myList as $element) { ?>
	<li><?php echo $element; ?></li>	
<?php } ?>
</ul>

<?php $content = ob_get_clean();
require_once 'template/template.php';
?>