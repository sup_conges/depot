<?php ob_start();
require_once 'includes/functions.php';
$title = "Calendriers";

function findOne($all_users, $id_booster) {
    foreach ($all_users as $user)
        if ($user->id_booster == $id_booster)
            return '<a href="user/profil?id=' . $user->id_booster . '">' . $user->firstname . " " . $user->lastname . '</a>';
}

function json_Event_Table($leaveTable) {
    $table = array();
    $i = 0;
    foreach ($leaveTable as $leave) {
        if(!empty($leave)){
        $user = User::getOne($leave->id_booster);
        $jsontable = array();
        $jsontable["title"] = "(".$user->id_booster.") - ".$user->getFullname();
        $jsontable["start"] = $leave->start;
        $jsontable["end"] = $leave->end;
        $table[$i] = $jsontable;}
        $i++;
    } return $table;
}

$currentYearHolidays = Holiday::getHolidays(date("Y"));
$nextYearHolidays = Holiday::getHolidays(date("Y", strtotime('+1 years')));

$holidays = array();
foreach ($currentYearHolidays as $holiday) {
    $holidayFullCalendar["title"] = $holiday->name;
    $holidayFullCalendar["start"] = date('Y-m-d', $holiday->timestamp);
    $holidayFullCalendar["end"] = date('Y-m-d', $holiday->timestamp);
    array_push($holidays, $holidayFullCalendar);
}

$cManagerLeaves = array();
foreach ($cManager as $user) {
    $leaveList = Leave::getAllWhere(["id_booster" => $user->id_booster, "status" => 1]);
    foreach ($leaveList as $leave){array_push($cManagerLeaves, $leave);}
}
$aManagerLeaves = array();
foreach ($aManager as $user) {
    $leaveList = Leave::getAllWhere(["id_booster" => $user->id_booster, "status" => 1]);
    foreach ($leaveList as $leave){array_push($aManagerLeaves, $leave);}
}
$aRegionLeaves = array();
foreach ($aRegion as $user) {
    $leaveList = Leave::getAllWhere(["id_booster" => $user->id_booster, "status" => 1]);
    foreach ($leaveList as $leave){array_push($aRegionLeaves, $leave);}
}
$pRegionLeaves = array();
foreach ($pRegion as $user) {
    $leaveList = Leave::getAllWhere(["id_booster" => $user->id_booster]);
    foreach ($leaveList as $leave){array_push($pRegionLeaves, $leave);}
}
$b_campusUsers = array();
foreach($a_campusUsers as $campus_users){
    if(!empty($campus_users)){
            array_push($b_campusUsers,json_Event_Table($campus_users));
    }else {
            array_push ($b_campusUsers, $campus_users);
    }
}

?>
<script>
    var currentPage = "Calendriers";
    var events = <?= json_encode(Leave::getForCalendar("leaves")); ?>;
    var holidays = <?= json_encode($holidays); ?>;
    var cManager = <?= json_encode(json_Event_Table($cManagerLeaves)); ?>;
    var aManager = <?= json_encode(json_Event_Table($aManagerLeaves)); ?>;
    var aRegion = <?= json_encode(json_Event_Table($aRegionLeaves)); ?>;
    var pRegion = <?= json_encode(json_Event_Table($pRegionLeaves)); ?>;
    var campusLeaves = <?= json_encode($b_campusUsers); ?>;
</script>

<h1><?= $title; ?></h1><hr/>

<div class="check">
    <?php foreach ($campus as $element){ ?>
        <label for="<?= $element->id?>">Campus de <?= $element->name?></label>
        <input type="checkbox" name="<?= $element->id?>" checked></br>
    <?php } ?>
</div>

<div id='calendar'></div>

<?php $content = ob_get_clean();
require_once 'template/template.php';
?>
