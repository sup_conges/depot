<?php ob_start();
require_once 'includes/functions.php';
$title = "modify user";
?>

<h1>User controller</h1>
<div id="edit_user_form">
    <form action="?ctrl=ajax&view=update_user&id=<?php echo $user->id_booster; ?>" method="POST">
        <fieldset>
            <legend><h2>Edition du compte: <?php echo $user->id_booster; ?></h2></legend>
            <label for="firstname">Prénom:</label>
            <input type="text" id="firstname" name="firstname" 
                   value="<?php echo $user->firstname; ?>"/><br/>
            <label for="lastname">Nom:</label>
            <input type="text" id="lastname" name="lastname" 
                   value="<?php echo $user->lastname; ?>"/><br/>
            <input type="hidden" id="id_booster" name="id_booster" 
                   value="<?php echo $user->id_booster; ?>"/>
            <label for="id_job">Poste</label>
            <select name="id_job">
                <option value="1" selected>AD: Assistant de direction</option> 
                <option value="2">CM: Campus manager</option>
                <option value="3">PR: President de region</option>
            </select></br>
            <label for="id_manager">ID Manager:</label>
            <input type="text" id="id_manager" name="id_manager" 
                   value="<?php echo $user->id_manager; ?>"/><br/>
            <label for="password">Mot de passe:</label>
            <input type="text" id="password" name="password" 
                   value="<?php echo $user->password; ?>"/><br/>
            <label for="campus">Campus:</label>
            <input type="text" id="campus" name="campus" 
                   value="<?php echo $user->campus; ?>"/><br/>     
            <label for="leave_day">jours de conges restant:</label>
            <input type="number" id="leave_day" name="leave_day" 
                   value="<?php echo $user->leave_day; ?>"/><br/>
            <label for="recovery_day">jours de recuperation restant:</label>
            <input type="number" id="recovery_day" name="recovery_day" 
                   value="<?php echo $user->recovery_day; ?>"/><br/>
            <input type="submit" value="Mettre a jour"/>
            <input type="reset" value="Reinitialiser"/>
        </fieldset>
    </form>
</div>

<?php $content = ob_get_clean();
require_once 'template/template.php';
?>