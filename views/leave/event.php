<?php ob_start();
require_once 'includes/functions.php';
$title = "Évènements";
?>

<script>
$(document).ready(function(){
    $("*[name='region[]']").change(function(){
        var region = [];
        for(var i=0;i<$("*[name='region[]']").length;i++){
            if($("*[name='region[]']")[i].checked)
                region.push($("*[name='region[]']")[i]);
        }
        $("*[name='campus[]']:checked").prop("checked",false);
        $(region).each(function(){
            $("."+this.id+"[name='campus[]']").prop("checked",true);
        });
    });
    $("*[name='campus[]']").change(function(){
        $("*"+this.id+"[name='region[]']").prop("checked",false);
    });

$("input[type='text']").change(function(){console.log(this.value)});

//datepicker
$(".datepicker").datepicker({
    dateFormat:'dd/mm/yy',
    showOtherMonths: true,
    selectOtherMonths: true,
    minDate: "+1d",
    maxDate: "+1Y",
    prevText: " ",
    nextText: " ",
    showAnim: "fold"});

});
</script>

<h1><?= $title; ?></h1><hr/>
<div class="wrap">
    <fieldset>
        <legend><h2>Liste des évènements</h2></legend>
        <?php if (!empty($events)) { ?>
            <table class="tablesorter">
                <thead>
                    <tr>
                        <th>N°</th>
                        <th>Catégorie</th>
                        <th>Date de l'évènement</th>
                        <th>Campus</th>
                        <th>Description</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($events)) { ?>
                        <?php $i = 0;
                        foreach($events as $event) { ?>
                                <tr>
                                <td><?= $event->id ?></td>
                                <td><?= $event->getCategory(); ?></td>
                                <td class="date"><?= $event->day ?></td>
                                <td class="show-campus"><?= $allEvents[$event->id]; ?></td>
                                <td class="show-description"><?= $event->description ?></td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                </tbody>
            </table>
        <?php } else { ?>
            <span>Aucun évènment disponible</span>
<?php } ?>
    </fieldset>
</div>


<?php if($me->id_job > 1){ ?>
<form action="../ajax/add_Event" method="POST">
    <fieldset>
        <legend><h2>Ajouter un évènement</h2></legend>
        <div class="wrap">
            <div class="inline">
                <div>
                    <select name="category" required>
                        <option value="" selected>Type d'évènement:</option>
                        <?php foreach ($category as $element) { ?>
                            <option value="<?= $element->id ?>"><?= $element->fullname ?></option>
                        <?php } ?>
                    </select>
                    <input type="text" name="day" class="datepicker" placeholder=" jj /mm /yyyy">
                </div>
                <div>
                    <textarea class="boxsizingBorder" style="width: 370px; height: 231px;" name="description" placeholder="Description de l'évènement..."></textarea>
                </div>
            </div>
            <div class="inline campus-region">
                <div>
                    <ul>
                    <?php foreach ($region as $element) { ?>
                        <li><input type="checkbox" id="<?= $element->id ?>" name="region[]" value="<?= $element->id ?>"><?= $element->region ?></li>
                    <?php } ?>
                    </ul>
                </div>
                <hr/>
                <div>
                    <ul>
                    <?php foreach ($campus as $element) { ?>
                        <li><input type="checkbox" class="<?= $element->id_region ?>" name="campus[]" value="<?= $element->id ?>"><?= $element->name ?></li>
                    <?php } ?>
                    </ul>
                </div>
                <hr/>
                <div>
                    <input type="submit" value="Enregistrer"/>
                    <input type="reset" value="Reinitialiser"/>
                </div>
            </div>

        </div>
    </fieldset>
</form>
<?php } ?>


<?php $content = ob_get_clean();
require_once 'template/template.php';
?>
