<?php ob_start();
require_once 'includes/functions.php';
$title = "Administration des congés";
?>

<h1><?= $title; ?></h1><hr/>
<div class="wrapper">
<div>
    <form action="?ctrl=ajax&view=leave_Treatment" method="POST">
        <fieldset>
            <legend><h2>Demandes de congés</h2></legend>
            <table class="leave-table tablesorter">
                <thead>
                    <tr>
                        <th>Employé</th>
                        <th>Début du congés</th>
                        <th>Fin du congés</th>
                        <th>JO</th>
                        <?php foreach ($leaveHierarchicalChain as $job => $userLink){ ?>
                            <th><?= $job ?></th>
                        <?php } ?>
                        <th>Date de la demande</th>
                        <th colspan="3">Actions</th>
                    </tr>
                </thead>
                <tbody>
            <?php if (!empty($leaveRequestsView)) { ?>
                <?php foreach ($leaveRequestsView as $requestView) { ?>
                    <tr>
                        <td><a href="../user/profil?id=<?= $requestView["id_booster"]; ?>" target="_blank"><?= $requestView["fullname"]; ?></a></td>
                        <td class="date"><?= $requestView["start"]; ?></td>
                        <td class="date"><?= $requestView["end"]; ?></td>
                        <td><?= $requestView["leave_point"]; ?></td>
                        <?php foreach($requestView as $key => $value){ ?>
                            <?php if(is_array($value)){ ?>
                                <?php if($value["date"]!=NULL) {?>
                                    <td class="liaison-leave-manager"><a href="../user/profil?id=<?= $value["id"]; ?>" target="_blank"><?= $value["fullname"]; ?></a><?= $value["date"] ?><img src="../includes/images/tab-valide-vert.png"/></td>
                                <?php } else if($value["id"]!=NULL) { ?>
                                    <td class="liaison-leave-manager"><a href="../user/profil?id=<?= $value["id"]; ?>" target="_blank"><?= $value["fullname"]; ?></a><spans> n'a pas encore validé</spans></td>
                                <?php } else echo '<td class="liaison-leave-manager"></td>' ?>
                            <?php } ?>
                        <?php } ?>
                        <td class="date"><?= $requestView["submit_date"]; ?></td>
                        <td>
                            <label style="color:green;">Valider</label>
                            <input type="radio" name="<?= $requestView["id"]; ?>" value="1" checked/>
                        </td>
                        <td>
                            <label style="color:red;">Refuser</label>
                            <input type="radio" name="<?= $requestView["id"]; ?>" value="0"/>
                        </td>
                        <td>
                            <label >En attente</label>
                            <input type="radio" name="<?= $requestView["id"]; ?>" value="2"/>
                        </td>
                    </tr>
                <?php } ?>
            <?php } else { ?>
                    <tr>
                        <td colspan="9">Vous n'avez pas de demande à traiter.</td>
                    </tr>
            <?php } ?>
                </tbody>
            <?php if (!empty($leaveRequestsView)) { ?>
                <tfoot>
                    <tr>
                        <td colspan="<?= 6+sizeof($leaveHierarchicalChain)-1; ?>">
                            <input type="submit" value="Envoyer"/>
                            <input type="reset" value="Reinitialiser"/>
                        </td>
                        <td><input type="button" class="select-all yes-input" value="All"/></td>
                        <td><input type="button" class="select-all no-input" value="All"/></td>
                        <td><input type="button" class="select-all standby-input" value="All"/></td>
                    </tr>
                </tfoot>
            <?php } ?>
            </table>
        </fieldset>
    </form>
</div>

<div>
    <form action="?ctrl=ajax&view=recovery_treatment" method="POST">
        <fieldset>
            <legend><h2>Demandes de récuperation</h2></legend>
                <table class="leave-table tablesorter">
                    <thead>
                        <tr>
                            <th>N°</th>
                            <th>Employé</th>
                            <th>Date du congé</th>
                            <th>Evènement utilisé</th>
                            <?php var_dump($recoveryHierarchicalChain); ?>
                            <?php foreach ($recoveryHierarchicalChain as $job => $userLink){ ?>
                                <th><?= $userLink ?></th>
                            <?php } ?>
                            <th>Date de la demande</th>
                            <th colspan="3">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                <?php if (!empty($recoveryRequestsView)) { ?>
                    <?php foreach ($recoveryRequestsView as $requestView) { ?>
                        <tr>
                            <td><?= $requestView["id"]; ?></td>
                            <td><a href="../user/profil?id=<?= $requestView["id_booster"]; ?>" target="_blank"><?= $requestView["fullname"]; ?></a></td>
                            <td class="date"><?= $requestView["day"]; ?></td>
                            <td class="show-event"><?= $requestView["id_event"]; ?></td>
                            <?php foreach($requestView as $key => $value){ ?>
                            <?php if(is_array($value)){ ?>
                                <?php if($value["date"]!=NULL) {?>
                                    <td class="liaison-leave-manager"><a href="../user/profil?id=<?= $value["id"]; ?>" target="_blank"><?= $value["fullname"]; ?></a><?= $value["date"] ?><img src="../includes/images/tab-valide-vert.png"/></td>
                                <?php } else if($value["id"]!=NULL) { ?>
                                    <td class="liaison-leave-manager"><a href="../user/profil?id=<?= $value["id"]; ?>" target="_blank"><?= $value["fullname"]; ?></a><spans> n'a pas encore validé</spans></td>
                                <?php } else echo '<td class="liaison-leave-manager"></td>' ?>
                            <?php } ?>
                        <?php } ?>
                            <td class="date"><?= $requestView["submit_date"]; ?></td>
                            <td>
                                <label style="color:green;">Valider</label>
                                <input type="radio" name="<?= $requestView["id"]; ?>" value="1"  checked/>
                            </td>
                            <td>
                                <label style="color:red;">Refuser</label>
                                <input type="radio" name="<?= $requestView["id"]; ?>" value="0"/>
                            </td>
                            <td>
                                <label >En attente</label>
                                <input type="radio" name="<?= $requestView["id"]; ?>" value="2"/>
                            </td>
                        </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr>
                        <td colspan="8">Vous n'avez pas de demande à traiter.</td>
                    </tr>
                <?php } ?>
                </tbody>
            <?php if (!empty($recoveryRequestsView)) { ?>
                <tfoot>
                    <tr>
                        <td colspan="<?= 6+sizeof($recoveryHierarchicalChain)-1; ?>">
                            <input type="submit" value="Envoyer"/>
                            <input type="reset" value="Reinitialiser"/>
                        </td>
                        <td><input type="button" class="select-all yes-input" value="All"/></td>
                        <td><input type="button" class="select-all no-input" value="All"/></td>
                        <td><input type="button" class="select-all standby-input" value="All"/></td>
                    </tr>
                </tfoot>
            <?php } ?>
            </table>
        </fieldset>
    </form>
</div>

<div>
    <form action="?ctrl=ajax&view=spr_Treatment" method="POST">
        <fieldset>
            <legend><h2>Participation aux evenement SPR</h2></legend>
            <table class="leave-table tablesorter">
                <thead>
                    <tr>
                        <th>N°</th>
                        <th>Employé</th>
                        <th>Date de l'évènement</th>
                        <th>Description</th>
                        <th>Attend d'être validé par</th>
                        <th>Date de la demande</th>
                        <th colspan="3">Actions</th>
                    </tr>
                </thead>
                <tbody>
                <?php if (!empty($sprEventRequestView)) { ?>
                    <?php foreach ($sprEventRequestView as $requestView) { ?>
                    <tr>
                        <td><?= $requestView["id"]; ?></td>
                        <td><a href="../user/profil?id=<?= $requestView["id_booster"]; ?>" target="_blank"><?= $requestView["fullname"]; ?></a></td>
                        <td class="date"><?= $requestView["day"]; ?></td>
                        <td class="show-description"><?= $requestView["id_event"]; ?></td>
                        <?php if($requestView["last_update"]!=NULL) {?>
                            <td><a href="../user/profil?id="<?= $requestView["manager"]["id"] ?>target="_blank"><?= $requestView["manager"]["fullname"]; ?></a> <?= $requestView["last_update"] ?><img src="../includes/images/tab-valide-vert.png"/></td>
                        <?php } else { ?>
                            <td><a href="../user/profil?id="<?= $requestView["manager"]["id"] ?> target="_blank"><?= $requestView["manager"]["fullname"]; ?></a> <spans>n'a pas encore validé</spans></td>
                        <?php } ?>
                        <td class="date"><?= $requestView["submit_date"]; ?></td>
                        <td>
                            <label style="color:green;">Présent</label>
                            <input type="radio" name="<?= $requestView["id"]; ?>" value="1" checked/>
                        </td>
                        <td>
                            <label style="color:red;">Absent</label>
                            <input type="radio" name="<?= $requestView["id"]; ?>" value="0"/>
                        </td>
                        <td>
                            <label >En attente</label>
                            <input type="radio" name="<?= $requestView["id"]; ?>" value="2"/>
                        </td>
                    </tr>
                    <?php } ?>
                <?php } else { ?>
                    <tr>
                        <td colspan="7">Vous n'avez pas de demande à traiter.</td>
                    </tr>
                <?php } ?>
                </tbody>
            <?php if (!empty($sprEventRequestView)) { ?>
                <tfoot>
                    <tr>
                        <td colspan="6">
                            <input type="submit" value="Envoyer"/>
                            <input type="reset" value="Reinitialiser"/>
                        </td>
                        <td><input type="button" class="select-all yes-input" value="All"/></td>
                        <td><input type="button" class="select-all no-input" value="All"/></td>
                        <td><input type="button" class="select-all standby-input" value="All"/></td>
                    </tr>
                </tfoot>
            <?php } ?>
            </table>
        </fieldset>
    </form>
</div>
</div>

<?php $content = ob_get_clean();
require_once 'template/template.php';
?>
