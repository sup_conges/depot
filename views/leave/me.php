<?php ob_start();
require_once 'includes/functions.php';
$title = "self user";
?>

<h1>Leave controller</h1>

<div class="formulaire">
    <form action="?ctrl=ajax&view=add_leave_Request" method="POST">
    <fieldset>
        <legend><h2>Leave</h2></legend>
        <label for="start_date">Premier jour de congé:</label>
        <input type="date" id="start_date" name="start_date" /><br/>
        <label for="end_date">Dernier jour de congé:</label>
        <input type="date" id="end_date" name="end_date" /><br/>
        <input type="submit" value="Envoyer la demande"/>
        <input type="reset" value="Reinitialiser"/>
    </fieldset>
    </form>
</div>
<div class="formulaire">
    <form action="?ctrl=ajax&view=add_leave" method="POST">
    <fieldset>
        <legend><h2>Recovery</h2></legend>
        <label for="start_date">Premier jour de recup:</label>
        <input type="date" id="start_date" name="start_date" /><br/>
        <label for="end_date">Dernier jour de recup:</label>
        <input type="date" id="end_date" name="end_date" /><br/>
        <input type="submit" value="Envoyer la demande"/>
        <input type="reset" value="Reinitialiser"/>
    </fieldset>
    </form>
</div>
<div class="formulaire">
    <form action="?ctrl=ajax&view=add_Request_SPR_Event" method="POST">
    <fieldset>
        <legend><h2>SPR_Event</h2></legend>
        <label for="start_date">Premier jour de l'évenement:</label>
        <input type="date" id="start_date" name="start_date" /><br/>
        <label for="end_date">Dernier jour de l'évenement:</label>
        <input type="date" id="end_date" name="end_date" /><br/>
        <label for="category">categorie:</label>
            <select name="category">
                <option value="1" selected>JPO: Journée porte ouverte</option> 
                <option value="2">FORUM</option>
                <option value="3">SALON</option>
            </select></br>
        <input type="submit" value="Envoyer la demande"/>
        <input type="reset" value="Reinitialiser"/>
    </fieldset>
    </form>
</div>

<div class="formulaire">
    <form action="?ctrl=ajax&view=add_Request_Recovery" method="POST">
    <fieldset>
        <legend><h2>Recovery</h2></legend>
        <label for="start_date">Premier jour de recupération:</label>
        <input type="date" id="start_date" name="start_date" /><br/>
        <label for="end_date">Dernier jour de récuperation:</label>
        <input type="date" id="end_date" name="end_date" /><br/>
        <label for="SPR_Event">Jour SPR de récupération</label>
             <select name ="SPR_recup">
                 <option value="0" selected>Selectionner un Evenement SPR</option>
                <?php foreach($myRSprEventList as $element) { ?>
                    <option value ="<?php echo $element->id ?>"> <?php echo $element->start_date ?></option>
                <?php } ?>
                 
             </select></br>
        <input type="submit" value="Envoyer la demande"/>
        <input type="reset" value="Reinitialiser"/>
    </fieldset>
    </form>
</div>

<?php $content = ob_get_clean();
require_once 'template/template.php';
?>