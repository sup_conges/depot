<?php ob_start();
require_once 'includes/functions.php';
$title = "Mes demandes";

function yearsHolidays($cYear,$nYear){
    foreach($nYear as $holiday){
        array_push($cYear, $holiday);
    } return $cYear;
} $holidays = yearsHolidays($currentYearHolidays, $nextYearHolidays);
?>

<script>
    var enable_Event_Category = <?= json_encode($a_category); ?>;
    var holidays = <?php echo json_encode($holidays) ?>;
    var category = <?= json_encode($a_category); ?>;
    var enable_Event_Category = <?= json_encode($a_events_category); ?>;
    var solde = <?= $me->getSolde(); ?>;

//events
    var eventTable = [
        <?= json_encode($eventJob); ?>,
        <?= json_encode($eventJpo); ?>,
        <?= json_encode($eventOther); ?>
    ];

    var myLeaves   = <?= json_encode($myLeavesRequests);   ?>;
    var myRecovery = <?= json_encode($myRecoveryRequests); ?>;
    var myAllEvent = <?= json_encode($mySubscribes); ?>;
</script>

<h1><?= $title; ?></h1><hr/>
<div class="form-add">
    <h2>Mes demandes de congés</h2>
    <a class="leave">Nouvelle demande</a>
</div>
<?php if (!empty($myLeaveRequestView)) { ?>
    <table class="tablesorter leave-table my-leaves request leave">
        <thead>
            <tr>
                <th>Début du congé</th>
                <th>Fin du congé</th>
                <?php foreach ($hierarchicalChain as $job => $userLink){ ?>
                  <?php if($userLink!=NULL) echo '<th>'.$job.'</th>'; ?>
                <?php } ?>
                <th>Date de la requete</th>
            </tr>
        </thead>
        <tbody>
            <?php if (!empty($myLeaveRequestView)) { ?>
                <?php foreach ($myLeaveRequestView as $requestView) { ?>
                    <tr>
                        <?php foreach($requestView as $key => $value){ ?>
                            <?php if(!is_array($value)){ ?>
                                <?php if($key=="delete"){ ?>
                                    <td class="cancel"><a id="<?= $requestView[$key]; ?>" class="delete">Annuler ma demande</a></td>
                                <?php } else { ?>
                                    <td class="date"><?= $requestView[$key] ?></td>
                                <?php } ?>
                            <?php } else { ?>
                                <?php if($value["date"]!=NULL) {?>
                                    <td class="manager-validation"><a href="../user/profil?id=<?= $value["id"]; ?>" target="_blank"><?= $value["fullname"]; ?></a> <?= $value["date"] ?><img src="../includes/images/tab-valide-vert.png"/></td>
                                <?php } else { ?>
                                    <td class="manager-validation"><a href="../user/profil?id=<?= $value["id"]; ?>" target="_blank"><?= $value["fullname"]; ?></a> <spans>n'a pas encore validé</spans></td>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    </tr>
                <?php } ?>
            <?php } ?>
        </tbody>
    </table>
<?php } else { ?>
    <span>Vous n'avez pas de demande de congé</span>
<?php } ?>

 <br/>

<div id="event-leave-request-form" class="request-form form leave">
    <form action="?ctrl=ajax&view=add_Leave" method="POST">
        <fieldset>
            <legend><a><h2>Nouvelle demande de congé</h2></a></legend>
            <table id="leave-form" class="myAllEvent">
                <thead>
                    <tr>
                        <th>Début du congé</th>
                        <th>Fin du congé</th>
                        <th>Jour Fériés</th>
                        <th>Durée totale</th>
                        <th>jours ouvrables</th>
                        <th>Nouveau solde</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><input type="text" name="start"  class="datepicker" placeholder=" jj /mm /yyyy"/></td>
                        <td><input type="text" name="end"  class="datepicker" placeholder=" jj /mm /yyyy"/></td>
                        <td>
                            <button type="button" id="button_feries" disabled=true>Voir les jours fériés</button>
                        </td>
                        <td><span id="dt">0</span> jours</td>
                        <td><span id="jo">0</span> jours</td>
                        <td><span id="ns"><?= $me->getSolde() ?></span></td>

                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="6">
                            <input type="submit" value="Envoyer la demande" disabled= true/>
                            <input type="reset" value="Reinitialiser"/>
                            
                            <div id="unpaid_leave">
                                <label for="unpaid_leave">Congés Sans Solde</label>
                                <input type="checkbox" name="unpaid_leave">
                            </div>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </fieldset>
    </form>
</div>

</br>

<h2>Mes congés validés</h2>
<?php if (!empty($myLeaveList)) { ?>
    <table class="tablesorter leave-table my-leaves accepted leave">
        <thead>
            <tr>
                <th>Début du congé</th>
                <th>Fin du congé</th>
                <?php foreach ($hierarchicalChain as $job => $userLink){ ?>
                    <th><?= $job ?></th>
                <?php } ?>
                <th>Date de la requete</th>
            </tr>
        </thead>
        <tbody>
            <?php if (!empty($myLeaveView)) { ?>
                <?php foreach ($myLeaveView as $view) { ?>
                    <tr>
                        <?php foreach($view as $key => $value){ ?>
                            <?php if(!is_array($value)){ ?>
                                <?php if($key=="delete"){ ?>
                                    <td class="cancel"><a id="<?= $view["delete"] ?>" class="delete">Annuler ma demande</a></td>
                                <?php } else { ?>
                                    <td class="date"><?= $view[$key] ?></td>
                                <?php } ?>
                            <?php } else { ?>
                                <?php if($value["date"]!=NULL) {?>
                                    <td class="manager-validation"><a href="../user/profil?id=<?= $value["id"]; ?>" target="_blank"><?= $value["fullname"]; ?></a> <?= $value["date"] ?><img src="../includes/images/tab-valide-vert.png"/></td>
                                <?php } else { ?>
                                    <td class="manager-validation"><a href="../user/profil?id=<?= $value["id"]; ?>" target="_blank"><?= $value["fullname"]; ?></a> <spans>n'a pas encore validé</spans></td>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    </tr>
                <?php } ?>
            <?php } ?>
        </tbody>
    </table>
<?php } else { ?>
    <span>Vous n'avez pas de demande de congé</span>
<?php } ?>

<hr/>
<div class="form-add">
    <h2>Mes demandes de récupération</h2>
    <a class="recovery">Nouvelle demande</a>
</div>
<?php if (!empty($myRecoveryRequestView)) { ?>
    <table class="tablesorter leave-table my-recovery request recovery">
        <thead>
            <tr>
                <th>Date du congé</th>
                <th>Evénement utilisé</th>
                <?php foreach ($hierarchicalChain as $job => $userLink){ ?>
                <th><?= $job ?></th>
                <?php } ?>
                <th>Date de la requete</th>
            </tr>
        </thead>
        <tbody>
            <?php if (!empty($myRecoveryRequestView)) { ?>
                <?php foreach ($myRecoveryRequestView as $requestView) { ?>
                    <tr>
                        <?php foreach($requestView as $key => $value){ ?>
                            <?php if(!is_array($value)){ ?>
                                <?php if($key=="delete"){ ?>
                                    <td class="cancel"><a id="<?= $requestView[$key]; ?>" class="delete">Annuler ma demande</a></td>
                                <?php } else if($key=="event"){ ?>
                                    <td class="show-event"><?= $requestView[$key];?></td>
                                <?php } else { ?>
                                    <td class="date"><?= $requestView[$key]; ?></td>
                                <?php } ?>
                            <?php } else { ?>
                                <?php if($value["date"]!=NULL) {?>
                                    <td class="manager-validation"><a href="../user/profil?id=<?= $value["id"] ?>" target="_blank"><?= $value["fullname"]; ?></a> <?= $value["date"] ?><img src="../includes/images/tab-valide-vert.png"/></td>
                                <?php } else { ?>
                                    <td class="manager-validation"><a href="../user/profil?id=<?= $value["id"] ?>" target="_blank"><?= $value["fullname"]; ?></a> <spans>n'a pas encore validé</spans></td>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    </tr>
                <?php } ?>
            <?php } ?>
        </tbody>
    </table>
<?php } else { ?>
    <span>Vous n'avez pas de demande de récupération</span>
<?php } ?>

<br/>

<div id="event-recovery-request-form" class="request-form form recovery">
    <form action="?ctrl=ajax&view=add_Recovery" method="POST">
        <fieldset>
            <legend><a><h2>Nouvelle demande de récupération</h2></a></legend>
            <table id="recovery-form">
                <thead>
                    <tr>
                        <th>Date du congé</th>
                        <th>Evénement à récupérer</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <input type="text" name="day" class="datepicker" placeholder=" jj /mm /yyyy">
                        </td>
                        <td>
                            <select name ="id_event" required>
                                <option value="" selected>Récupération du:</option>
                            <?php foreach ($enablesEventsView as $enableEventView) { ?>
                                <option value ="<?= $enableEventView["id_event"] ?>"><?= $enableEventView["category"]." ".$enableEventView["day"]." ".$enableEventView["description"]; ?></option>
                            <?php } ?>
                            </select>
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td>
                            <input type="submit" value="Envoyer la demande" disabled= true/>
                            <input type="reset" value="Reinitialiser"/>
                        </td>
                        <td>
                            <span id="recup_valide"></span>
                            <img id ="invalid" src="../includes/images/invalid.png" alt=" invalide!" hidden/>
                        </td>
                    </tr>
                </tfoot>
            </table>

        </fieldset>
    </form>
</div>

<br/>

<h2>Mes récupérations validés</h2>
<?php if (!empty($myRecoveryList)) { ?>
    <table class="tablesorter leave-table my-recovery accepted recovery">
        <thead>
            <tr>
                <th>Date du congé</th>
                <th>Evénement utilisé</th>
                <?php foreach ($hierarchicalChain as $job => $userLink){ ?>
                <th><?= $job ?></th>
                <?php } ?>
                <th>Date de la requete</th>
            </tr>
        </thead>
        <tbody>
            <?php if (!empty($myRecoveryView)) { ?>
                <?php foreach ($myRecoveryView as $view) { ?>
                    <tr>
                        <td class="date"><?= $view["date"]; ?></td>
                        <td class="show-event"><?= $view["event"]; ?></td>
                        <?php foreach($view as $key => $value){ ?>
                            <?php if(is_array($value)){ ?>
                                <?php if($value["date"]!=NULL) {?>
                                    <td class="manager-validation"><a href="../user/profil?id=<?= $value["id"] ?>" target="_blank"><?= $value["fullname"]; ?></a> <?= $value["date"] ?><img src="../includes/images/tab-valide-vert.png"/></td>
                                <?php } else { ?>
                                    <td class="manager-validation"><a href="../user/profil?id=<?= $value["id"] ?>" target="_blank"><?= $value["fullname"]; ?></a> <spans>n'a pas encore validé</spans></td>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                        <td class="date"><?= $view["submit_date"]; ?></td>
                    </tr>
                <?php } ?>
            <?php } ?>
        </tbody>
    </table>
<?php } else { ?>
    <span>Vous n'avez pas de demande de récupération</span>
<?php } ?>

<hr/>

<div class="form-add">
    <h2>Mes inscriptions aux événements</h2>
    <a class="inscription">S'inscrire à un événement</a>
</div>
<?php if (!empty($mySprEventRequestView)) { ?>
    <table class="tablesorter leave-table inscription">
        <thead>
            <tr>
                <th>Categorie</th>
                <th>Date de l'évènement</th>
                <th>Descripton</th>
                <th>Attend d'être validé par</th>
                <th>Date de la requete</th>
            </tr>
        </thead>
        <tbody>
            <?php if (!empty($mySprEventRequestView)) { ?>
                <?php foreach ($mySprEventRequestView as $requestView) { ?>
                    <tr>
                        <td><?= $requestView["category"]; ?></td>
                        <td class="date"><?= $requestView["date"]; ?></td>
                        <td class="show-description"><?= $requestView["description"];?></td>
                        <td class="manager-validation"><a href="../user/profil?id=<?= $requestView["manager"]["id"]; ?>" target="_blank"><?= $requestView["manager"]["fullname"]; ?></a> <spans>n'a pas encore validé</spans></td>
                        <td class="date"><?= $requestView["submit_date"]; ?></td>
                        <td class="cancel"><a id="<?= $requestView["id"]; ?>" class="delete">Annuler ma demande</a></td>
                    </tr>
                <?php } ?>
            <?php } ?>
        </tbody>
    </table>
<?php } else { ?>
    <span>Vous n'avez pas de demande de participation à un événement</span>
<?php } ?>

</br>

<div id="event-suscribe-form" class="request-form form inscription">
    <form action="../ajax/subscribe" method="POST">
        <fieldset>
            <legend><a><h2>S'inscrire à un événement</h2></a></legend>
            <table>
                <thead>
                    <tr>
                        <th>Categorie</th>
                        <th>Date de l'évenement</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <select name="category">
                                <option value="" selected>Type d'événement:</option>
                            <?php foreach ($a_category as $element){ ?>
                                <option value="<?= $element->id ?>"><?= $element->fullname ?></option>
                            <?php } ?>
                            </select>
                        </td>
                        <td>
                            <select name="id_event" required>
                                <?php if(empty($a_events_category)){ ?>
                                    <option value="">Aucun événement disponible.</option>
                                <?php } else { ?>
                                    <option value="" selected>Selectionnez un événement:</option>
                                    <?php foreach ($enableEvents as $event){ ?>
                                        <option value="<?= $event->id ?>"><?= $event->description." ".$event->day ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="4">
                            <input type="submit" value="S'inscrire"/>
                            <input type="reset" value="Reinitialiser"/>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </fieldset>
    </form>
</div>

<h2>Mes participations aux événements</h2>
<?php if (!empty($myparticipationsView)) { ?>
    <table class="tablesorter leave-table myAllEvent inscription">
        <thead>
            <tr>
                <th>Categorie</th>
                <th>Date de l'évènement</th>
                <th>Descripton</th>
                <th>Date de la validation</th>
                <th>Date de la requete</th>
            </tr>
        </thead>
        <tbody>
            <?php if (!empty($myparticipationsView)) { ?>
                <?php foreach ($myparticipationsView as $participation) { ?>
                    <tr>
                        <td><?= $participation["category"]; ?></td>
                        <td class="date"><?= $participation["date"]; ?></td>
                        <td class="show-description"><?= $participation["description"]; ?></td>
                        <td class="manager-validation"><a href="../user/profil?id=<?= $participation["manager"]["id"]; ?>" target="_blank"><?= $participation["manager"]["fullname"]; ?></a> <?= $participation["last_update"] ?><img src="../includes/images/tab-valide-vert.png"/></td>
                        <td class="date"><?= $participation["submit_date"]; ?></td>
                    </tr>
                <?php } ?>
            <?php } ?>
        </tbody>
    </table>
<?php } else { ?>
    <span>Vous n'êtes inscrit a aucun événement</span>
<?php } ?>

<?php $content = ob_get_clean();
require_once 'template/template.php';
?>
