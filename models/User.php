<?php

class User extends Model {
    public $firstname = "";
    public $lastname = "";
    public $id_booster;
    public $id_job;
    public $id_manager = null;
    public $password;
    public $email;
    public $leave_day=30;
    public $saturday=6;

    public function __construct() {
        if (!self::$_pdo) self::connect();
    }

    public static function getAllWhere($where, $table = "users", $class="User") {
        return parent::getAllWhere($where, $table, $class);
    }
    public static function getAll($table = "users", $class = "User") {
        return parent::getAll($table, $class);
    }
    public static function getOneWhere($where, $table = "users", $class="User") {
        return parent::getOneWhere($where, $table, $class);
    }
    public static function getOne($value, $table="users", $col="id_booster", $class="User") {
        return parent::getOne($value, $table, $col, $class);
    }

    public function save($table = "users") {
        return parent::save($table);
    }
    public function update($table = "users", $col="id_booster"){
        return parent::update($table, $col);
    }
    public function delete($table="users", $col="id_booster") {
        return parent::delete($table, $col);
    }
    public function getFullname(){
        if($this->firstname != "" && $this->lastname != "")
            return $this->firstname." ".$this->lastname;
        else return "Undefined";
    }
    public function getLink(){
        return '<a href="../user/profil?id=' . $this->id_booster . '" target="_blank">'.$this->getFullname().'</a>';
    }
    public function getJob(){
        return Job::getOne($this->id_job)->job;
    }
    public function getCampus(){
        $txt="";
        foreach(User_Campus::getAllWhere(["id_user" => $this->id_booster]) as $liaison){
            $txt .= Campus::getOne($liaison->id_campus)->name.", ";
        } return rtrim($txt, ", ").".";
    }
    public function getSolde(){
        $i_workingDays = 0;
        foreach(Leave::getAllWhere(["id_booster" => $this->id_booster, "unpaid_leave" => 0]) as $leave){
            $i_workingDays += date_Compare($leave->start, $leave->end)["days"];
        } return ($this->leave_day - $i_workingDays);
    }
}

class User_Campus extends Model{
    public $id;
    public $id_user;
    public $id_campus;

    public function save($table="user_campus") {
        return parent::save($table);
    }
    public static function getAllWhere($where, $table="user_campus", $class="User_Campus") {
        return parent::getAllWhere($where, $table, $class);
    }

}

class Job extends model {
    public $id;
    public $job;

    public static function getAllWhere($where, $table = "jobs", $class="Job") {
        return parent::getAllWhere($where, $table, $class);
    }
    public static function getAll($table = "jobs", $class = "Job") {
        return parent::getAll($table, $class);
    }
    public static function getOneWhere($where, $table = "jobs", $class="Job") {
        return parent::getOneWhere($where, $table, $class);
    }
    public static function getOne($value, $table="jobs", $col="id", $class="Job") {
        return parent::getOne($value, $table, $col, $class);
    }
}
