﻿<?php

class Campus extends model{
    public $id;
    public $name;
    public $id_region;

    public function __construct() {
        if (!self::$_pdo) self::connect();
    }
    
    public static function getAllWhere($where, $table, $class) {
        return parent::getAllWhere($where, $table, $class);
    }
    public static function getAll($table="campus", $class="Campus") {
        return parent::getAll($table, $class);
    }
    
    public static function getOneWhere($where, $table, $class) {
        return parent::getOneWhere($where, $table, $class);
    }
    public static function getOne($value, $table= "campus", $col="id", $class = "Campus") {
        return parent::getOne($value, $table, $col, $class);
    }
    public function save($table = "campus") {
        return parent::save($table);
    }
    public function update($table = "campus", $col="holidays"){
        return parent::update($table, $col);
    }
}

class Campus_Event extends model{
    public $id;
    public $id_campus;
    public $id_event;
    
    public function save($table="campus_events"){
        return parent::save($table);
    }
    public static function getAllWhere($where, $table="campus_events", $class="Campus_Event") {
        return parent::getAllWhere($where, $table, $class);
    }
    public static function getOne($value, $col="id", $table="campus_events", $class="Campus_Event") {
        return parent::getOne($value, $table, $col, $class);
    }
    public static function last_Entry($table="spr_events") {
        return parent::last_Entry($table);
    }
}

class Campus_Holiday{
    public $id;
    public $id_campus;
    public $timestamp_holiday;
    
    public function save($table = "campus_holidays") {
        return parent::save($table);
    }
    public function update($table = "campus_holidays", $col = "holidays"){
        return parent::update($table, $col);
    }
    public function delete($table="campus_holidays", $col="holiday") {
        return parent::delete($table, $col);
    }
    public static function getAll($table="campus_holidays", $class="Campus_Holidays") {
        return parent::getAll($table, $class);
    }
    public static function getAllWhere($where, $table="campus_holidays", $class="Campus_Holidays") {
        return parent::getAllWhere($where, $table, $class);
    }
    public static function getOneWhere($where, $table = "campus_holidays", $class="Campus_Holidays") {
        return parent::getOneWhere($where, $table, $class);
    }
    public static function getOne($value, $table="campus_holidays", $col="holiday", $class="Campus_Holidays") {
        return parent::getOne($value, $table, $col, $class);
    }
}

