<?php

class Leave_Model extends Model {
    public $id;
    public $id_booster;
    public $submit_date;
    public $status=0;
    public $unpaid_leave=0;

    public function __construct() {
        if (!self::$_pdo) self::connect();
    }

    public static function getAllWhere($where, $table, $class) {
        return parent::getAllWhere($where, $table, $class);
    }
    public static function getAll($table, $class) {
        return parent::getAll($table, $class);
    }

    public static function getOneWhere($where, $table, $class) {
        return parent::getOneWhere($where, $table, $class);
    }
    public static function getOne($value, $table, $col, $class) {
        return parent::getOne($value, $table, $col, $class);
    }

    public function save($table){
        return parent::save($table);
    }
    public function update($table, $col){
        return parent::update($table, $col);
    }
    public function delete($table, $col) {
        return parent::delete($table, $col);
    }

    public static function getForCalendar($table){
        if (!self::$_pdo) self::connect();
        return self::$_pdo->query("SELECT CONCAT(U.firstname, ' ', U.lastname) as title, start, DATE_ADD(end, INTERVAL 1 DAY) as end
            FROM ".$table."
            LEFT JOIN users U ON U.id_booster = " . $table . ".id_booster
            ")->fetchAll(PDO::FETCH_ASSOC);
    }
}

class Leave extends Leave_Model{
    public $start;
    public $end;

    public function save($table="leaves"){
        return parent::save($table);
    }
    public function update($table="leaves", $col="id"){
        return parent::update($table, $col);
    }
    public function delete($table="leaves", $col="id") {
        return parent::delete($table, $col);
    }
    public static function getOneWhere($where, $table = "leaves", $class="Leave") {
        return parent::getOneWhere($where, $table, $class);
    }
    public static function getOne($value, $table="leaves", $col="id", $class="Leave") {
        return parent::getOne($value, $table, $col, $class);
    }
    public static function getAllWhere($where, $table="leaves", $class="Leave") {
        return parent::getAllWhere($where, $table, $class);
    }
    public static function getAll($table="leaves", $class="Leave") {
        return parent::getAll($table, $class);
    }
    public function getDuration(){
      $days = date_Compare($this->start,$this->end)["days"];
      if($this->unpaid_leave==0) return $days;
      else return "X";
    }
}

class Recovery extends Leave_Model{
    public $day;
    public $id_event;
    public $last_update;

    public function save($table="recovery"){
        return parent::save($table);
    }
    public function update($table="recovery", $col="id"){
        return parent::update($table, $col);
    }
    public function delete($table="recovery", $col="id") {
        return parent::delete($table, $col);
    }
    public static function getAll($table="recovery", $class="Recovery") {
        return parent::getAll($table, $class);
    }
    public static function getAllWhere($where, $table="recovery", $class="Recovery") {
        return parent::getAllWhere($where, $table, $class);
    }
    public static function getOneWhere($where, $table = "recovery", $class="Recovery") {
        return parent::getOneWhere($where, $table, $class);
    }
    public static function getOne($value, $table="recovery", $col="id", $class="Recovery") {
        return parent::getOne($value, $table, $col, $class);
    }

}

class Subscribe_Spr_Event extends Leave_Model{
    public $id_event;

    public function save($table="users_spr_events"){
        return parent::save($table);
    }
    public function update($table="users_spr_events", $col="id"){
        return parent::update($table, $col);
    }
    public function delete($table="users_spr_events", $col="id") {
        return parent::delete($table, $col);
    }
    public static function getAll($table="users_spr_events", $class="Subscribe_Spr_Event") {
        return parent::getAll($table, $class);
    }
    public static function getAllWhere($where, $table="users_spr_events", $class="Subscribe_Spr_Event") {
        return parent::getAllWhere($where, $table, $class);
    }
    public static function getOneWhere($where, $table = "users_spr_events", $class="Subscribe_Spr_Event") {
        return parent::getOneWhere($where, $table, $class);
    }
    public static function getOne($value, $table="users_spr_events", $col="id", $class="Subscribe_Spr_Event") {
        return parent::getOne($value, $table, $col, $class);
    }

}
class Leave_Treatment_Date extends Model{

    public static function getAllWhere($where, $table="leave_treatment_date", $class="Leave_Treatment_Date") {
        return parent::getAllWhere($where, $table, $class);
    }
    public static function getAll($table="leave_treatment_date", $class="Leave_Treatment_Date") {
        return parent::getAll($table, $class);
    }
    public static function getOneWhere($where, $table = "leave_treatment_date", $class="Leave_Treatment_Date") {
        return parent::getOneWhere($where, $table, $class);
    }
    public static function getOne($value, $table="leave_treatment_date", $col="id", $class="Leave_Treatment_Date") {
        return parent::getOne($value, $table, $col, $class);
    }
    public function save($table="leave_treatment_date"){
        return parent::save($table);
    }
    public function update($table="leave_treatment_date", $col="id"){
        return parent::update($table, $col);
    }
    public function delete($table="leave_treatment_date", $col="id"){
        return parent::delete($table, $col);
    }
}
class Recovery_Treatment_Date extends Model{

    public static function getAllWhere($where, $table="recovery_treatment_date", $class="Recovery_Treatment_Date") {
        return parent::getAllWhere($where, $table, $class);
    }
    public static function getAll($table="recovery_treatment_date", $class="Recovery_Treatment_Date") {
        return parent::getAll($table, $class);
    }
    public static function getOneWhere($where, $table = "recovery_treatment_date", $class="Recovery_Treatment_Date") {
        return parent::getOneWhere($where, $table, $class);
    }
    public static function getOne($value, $table="recovery_treatment_date", $col="id", $class="Recovery_Treatment_Date") {
        return parent::getOne($value, $table, $col, $class);
    }
    public function save($table="recovery_treatment_date"){
        return parent::save($table);
    }
    public function update($table="recovery_treatment_date", $col="id"){
        return parent::update($table, $col);
    }
    public function delete($table="recovery_treatment_date", $col="id"){
        return parent::delete($table, $col);
    }
}
