<?php

class SPR_Event extends model{
    public $id;
    public $day;
    public $id_category;
    public $description="";
	
	public function __construct() {
        if (!self::$_pdo) self::connect();
    }
    public function save($table="spr_events"){
        return parent::save($table);
    }
    
    public static function getAllWhere($where, $table="spr_events", $class="SPR_Event") {
        return parent::getAllWhere($where, $table, $class);
    }
    
    public static function getAll($table="spr_events", $class="SPR_Event") {
        return parent::getAll($table, $class);
    }
    
    public static function getOne($value, $table="spr_events", $col="id", $class="SPR_Event") {
        return parent::getOne($value, $table, $col, $class);
    }
    
    public static function getOneWhere($where, $table="spr_events", $class="SPR_Event") {
        return parent::getOneWhere($where, $table, $class);
    }
    public static function last_Entry($table="spr_events"){
        return parent::last_Entry($table);
    }
    public function getCategory(){
        return Category::getOne($this->id_category)->category;
    }
    public static function getCampus(){
        $txt="";
        foreach(Campus_Event::getAllWhere(["id_event" => $this->id]) as $liaison){
            $txt .= Campus::getOne($liaison->id_campus)->name.", ";
        } return rtrim($txt, ", ").".";
    }
}

class Category extends model{
    public $id;
	public $category;
	public $fullname;
    
    public function __construct() {
        if (!self::$_pdo) self::connect();
    }
    
    public static function getAllWhere($where, $table="category", $class="Category") {
        return parent::getAllWhere($where, $table, $class);
    }
    
    public static function getAll($table="category", $class="Category") {
        return parent::getAll($table, $class);
    }
    
    public static function getOneWhere($where, $table="category" ,$class="Category") {
        return parent::getOneWhere($where, $table, $class);
    }
    public static function getOne($value, $table="category", $col="id", $class="Category") {
        return parent::getOne($value, $table, $col, $class);
    }
}