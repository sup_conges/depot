<?php

class Region extends model{
    public $id;
    public $region;
    public $id_ar;

    public function __construct() {
        if (!self::$_pdo) self::connect();
    }
    
    public static function getAllWhere($where,$table="region",$class="Region") {
        return parent::getAllWhere($where, $table, $class);
    }
    public static function getAll($table="region", $class="Region") {
        return parent::getAll($table, $class);
    }
    public static function getOneWhere($where, $table="region", $class="Region") {
        return parent::getOneWhere($where, $table, $class);
    }
}