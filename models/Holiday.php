<?php

class Holiday extends Model{
    public $name;
    public $timestamp;
    
    public function save($table="holidays") {
        return parent::save($table);
    }
    public function update($table="holidays", $col="id"){
        return parent::update($table, $col);
    }
    public static function getAll($table="holidays", $class ="Holiday") {
        return parent::getAll($table, $class);
    }
    public static function getOne($value, $col="id", $table="holidays", $class = "Holiday") {
        return parent::getOne($value, $table, $col, $class);
    }
    
    public static function getHolidays($year = null){
        if ($year === null) $year = intval(strftime('%Y'));
        $easterDate = easter_date($year);
        $easterDay = date('j', $easterDate);
        $easterMonth = date('n', $easterDate);
        $easterYear = date('Y', $easterDate);
        $holidays = array();
        $holiday = new Holiday();
        $holiday->name = "Nouvel An";
        $holiday->timestamp = mktime(0, 0, 0, 1, 1, $year);
        array_push($holidays, $holiday);
        $holiday = new Holiday();
        $holiday->name = "Fête du travail";
        $holiday->timestamp = mktime(0, 0, 0, 5, 1, $year);
        array_push($holidays, $holiday);
        $holiday = new Holiday();
        $holiday->name = "8 mai";
        $holiday->timestamp = mktime(0, 0, 0, 5, 8, $year);
        array_push($holidays, $holiday);
        $holiday = new Holiday();
        $holiday->name = "Fête Nationale";
        $holiday->timestamp = mktime(0, 0, 0, 7, 14, $year);
        array_push($holidays, $holiday);
        $holiday = new Holiday();
        $holiday->name = "Assomption";
        $holiday->timestamp = mktime(0, 0, 0, 8, 15, $year);
        array_push($holidays, $holiday);
        $holiday = new Holiday();
        $holiday->name = "Toussaint";
        $holiday->timestamp = mktime(0, 0, 0, 11, 1, $year);
        array_push($holidays, $holiday);
        $holiday = new Holiday();
        $holiday->name = "Armistice";
        $holiday->timestamp = mktime(0, 0, 0, 11, 11, $year);
        array_push($holidays, $holiday);
        $holiday = new Holiday();
        $holiday->name = "Noël";
        $holiday->timestamp = mktime(0, 0, 0, 12, 25, $year);
        array_push($holidays, $holiday);
        $holiday = new Holiday();
        $holiday->name = "lundi de Paques";
        $holiday->timestamp = mktime(0, 0, 0, $easterMonth, $easterDay + 1, $easterYear);
        array_push($holidays, $holiday);
        $holiday = new Holiday();
        $holiday->name = "Ascension";
        $holiday->timestamp = mktime(0, 0, 0, $easterMonth, $easterDay + 39, $easterYear);
        array_push($holidays, $holiday);
        $holiday = new Holiday();
        $holiday->name = "Pentecote";
        $holiday->timestamp = mktime(0, 0, 0, $easterMonth, $easterDay + 50, $easterYear);
        array_push($holidays, $holiday);
        $holiday = new Holiday();
        $holiday->name = "Vendredi Saint";
        $holiday->timestamp = mktime(0,0,0,$easterMonth, $easterDay - 2, $easterYear);
        array_push($holidays, $holiday);
        $holiday = new Holiday();
        $holiday->name = "Saint étienne";
        $holiday->timestamp = mktime(0,0,0,12,26,$year);
        array_push($holidays, $holiday);
        return $holidays;
    }
    public static function holidays_to_Base(array $holidays){
        foreach($holidays as $holiday){
            $request = Holiday::getOne($holiday->timestamp, "timestamp");
            if(empty($request)) $holiday->save();
        }
    }
}
Holiday::holidays_to_Base(Holiday::getHolidays());
Holiday::holidays_to_Base(Holiday::getHolidays(date("Y", strtotime('+1 years'))));
