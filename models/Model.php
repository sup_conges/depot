<?php

abstract class Model {
    protected static $_pdo;

    protected static function connect() {
        self::$_pdo = new PDO("mysql:dbname=".DB_name.";host=".DB_host, DB_user, DB_password, array(
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
        ));
    }

    protected static function last_Entry($table){
        if (!self::$_pdo) self::connect();
        var_dump("SELECT LAST_INSERT_ID() FROM ".$table);
        return self::$_pdo->query("SELECT LAST_INSERT_ID() FROM ".$table)->fetch(PDO::FETCH_NUM)[0];
    }

    protected static function getAllWhere($where, $table, $class) {
        if (!self::$_pdo) self::connect();
        $sql = "SELECT * FROM `".$table."` WHERE ";
        foreach($where as $key => $value){
            $sql .= $key." = ".$value;
            $sql .= " AND ";
        } $sql = rtrim($sql, " AND ");
       return self::$_pdo->query($sql)->fetchAll(PDO::FETCH_CLASS, $class);
    }

    protected static function getAll($table, $class) {
        if (!self::$_pdo) self::connect();
        return self::$_pdo->query("SELECT * FROM `".$table."`")->fetchAll(PDO::FETCH_CLASS, $class);
    }

    protected static function getOneWhere($where, $table, $class) {
        if (!self::$_pdo) self::connect();
        $sql = "SELECT * FROM `".$table."` WHERE ";
        foreach($where as $key => $value){
            $sql .= $key." = '".$value."'";
            $sql .= " AND ";
        } $sql = rtrim($sql, " AND ");;
        $query = self::$_pdo->query($sql)->fetchAll(PDO::FETCH_CLASS, $class);
        if(isset($query[0])) return $query[0];
        else return $query;
    }
    protected static function getOne($value, $table, $col, $class) {
        $where = [$col => $value];
        return Model::getOneWhere($where, $table, $class);
    }

    public function save($table) {
        if (!self::$_pdo) self::connect();
        $sql = "INSERT INTO `".$table."` (";
        foreach ($this as $key => $value){
            if($key!="id" && $key!="submit_date") $sql .= $key.", ";
        }
        $sql = rtrim($sql, ", ");
        $sql .= ") VALUES (";
        foreach ($this as $key => $value){
            if($key!="id" && $key!="submit_date") $sql .= ":".$key.", ";
        }
        $sql = rtrim($sql, ", ");
        $sql .= ");";
        $stmt = self::$_pdo->prepare($sql);
        $exec_Table = array();
        foreach ($this as $key => $value){
            if($key!="id" && $key!="submit_date"){
                $sql_key = ":".$key;
                $exec_Table[$sql_key] = $this->$key;
            }
        }
        var_dump($sql);
        $stmt->execute($exec_Table);
    }

   public function update($table, $col){
       if (!self::$_pdo) self::connect();
        $sql = "UPDATE `".$table."` SET ";
        foreach ($this as $key => $value)
            if($key!="id") $sql .= $key."=:".$key.", ";
        $sql = rtrim($sql,", ");
        $sql .= " WHERE ".$col."=:".$col.";";
        $stmt = self::$_pdo->prepare($sql);
        $exec_Table = array();
        foreach ($this as $key => $value){
            if($key!="id" || $col=="id") $sql_key = ":".$key;
            $exec_Table[$sql_key] = $this->$key;
        }
        var_dump($sql);
        $stmt->execute($exec_Table);
    }

    protected function delete($table, $col){
        if (!self::$_pdo) self::connect();
        if(!isset($this->id)) $this->id = $this->id_booster;
        $sql = "DELETE FROM `".$table."` WHERE `".$col."` = ".$this->id;
        var_dump($sql);
        self::$_pdo->exec($sql);
    }
}
