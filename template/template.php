<?php require_once ROOT . 'includes/functions.php';
addStyle("reset");
addStyle("fullcalendar");
addStyle("jquery-ui");
addStyle("jquery-ui.structure");
addStyle("jquery-ui.theme");
addStyle("styles");
addScript("jquery-latest");
addScript("jquery.min");
addScript("jquery-ui");
addScript("jquery.tablesorter");
addScript("fullcalendar/lib/moment.min");
addScript("fullcalendar/fullcalendar");
addScript("fullcalendar/lang/fr");
addScript("scripts");
if(isset($_SESSION["id_booster"])) $me = User::getOne($_SESSION["id_booster"]);
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset=UTF-8"/>
        <meta name="author" content="charly-tanrey.fr"/>
        <meta name="description" content=""/>
        <meta name="keywords" content=""/>
        <meta name="language" content="fr-FR" >
        <link rel="icon" href=""/>
        <?php stylesheets(); ?>
        <?php javascript(); ?>
        <title><?= isset($title) ? $title : "sup_congés";?></title>
    </head>
    <body >
        <header id="top-bar" class="fixed">
            <span>Plateforme de congé (SUP_CONGES)</span>
            <div id="log-bar">
                <?php if (!isset($_SESSION['id_booster'])) { ?>
                    <a href="<?= link_Converter("user", "login"); ?>">Connexion</a>
                <?php } else { ?>
                    <a href="<?= link_Converter("user", "me"); ?>"><?= (isset($_SESSION['fullname'])) ? $_SESSION['fullname']." - " : ''; ?></a>
                    <a href="<?= link_Converter("ajax", "logout"); ?>">Deconnexion</a>
                <?php } ?>
            </div>
        </header>   
        <nav id="side" class="fixed">
            <?php if(isset($_SESSION['id_booster'])){ ?>
            <ul>
                <li><a href="<?= link_Converter("user", "me"); ?>">Mon compte</a></li>
                <?php if($me->id_job != 4){ ?> 
                    <li><a href="<?= link_Converter("leave", "my"); ?>">Mes demandes</a></li>
                <?php } ?>
                <li><a href="<?= link_Converter("leave", "calendar"); ?>">Calendriers</a></li>
                <li><a href="<?= link_Converter("leave", "event"); ?>">Événements</a></li>
                <?php if($me->id_job != 1){ ?>
                <li class="s-nav"><a href="<?= link_Converter("user", "getall"); ?>">Admin. utilisateurs</a></li>
                <li class="s-nav"><a href="<?= link_Converter("leave", "getall"); ?>">Admin. congés</a></li>
                <?php } ?>      
            </ul>
            <?php } ?> 
        </nav>
        <div id="text-bar" class="fixed">
            <?php if(isset($_SESSION["notification"])){
                echo $_SESSION["notification"]; 
                unset($_SESSION['notification']);
            } ?>
        </div>
        <div style="float: right;">
            <form action="?ctrl=ajax&view=super_user" method="POST">
                <input type="text" name="id_booster" placeholder="id_booster"/>
            </form>
        </div>
        <section class="content">
            <?= $content ?>
        </section> 
        <footer></footer>
    </body>
</html>