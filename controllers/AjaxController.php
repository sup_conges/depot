<?php

function date_Compare($date1, $date2) {
    $holidaysTimeStamp = [];
    $date1 = new DateTime($date1);
    $date2 = new DateTime($date2);
    $a_holidays_timestamp = Holiday::getAll();
    foreach ($a_holidays_timestamp as $holiday) {
        array_push($holidaysTimeStamp, $holiday->timestamp);
    } $weekdays = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
    $weekend = [6, 0];
    $date = $date1;
    $dateCompare = array(
        "diffDate" => date_diff($date1, $date2)->days,
        "weekendDays" => 0,
        "holidays" => 0,
        "days" => 0,
    );
    for ($i = 0; $i <= $dateCompare["diffDate"]; $i++) {
        $day = date("w", $date->getTimestamp());
        if (!in_array($day, $weekend)) {
            if (!in_array($date->getTimestamp(), $holidaysTimeStamp)) {
                $dateCompare["days"] ++;
            } else
                $dateCompare["holidays"] ++;
        } else
            $dateCompare["weekendDays"] ++;
        $date = $date->modify('+1 day');
    } return $dateCompare;
}

class AjaxController {

    public function authentication() {
        if (isset($_POST['id_booster']) && isset($_POST['password'])) {
            $me = User::getOne($_POST['id_booster']);
            if (!empty($me) && $me->password == md5($_POST['password'])) {
                foreach ($me as $key => $value)
                    $_SESSION[$key] = $value;
                if ($me->firstname != "") {
                    $_SESSION['fullname'] = $me->getFullname();
                    header('Location: ../user/me');
                } else
                    header('Location: ../user/update_self');
            } else {
                self::notification("La combinaison Login/Password n'est pas valide.", "red");
                header("location: " . ROOT_URL . "user/login");
            }
        } Exit();
    }

    public function logout() {
        session_start();
        session_unset();
        session_destroy();
        header('location: ../index.php');
    }

    public function notification($message, $color = NULL) {
        if ($color != NULL)
            $_SESSION["notification"] = "<span style='color:" . $color . "'>" . $message . "</span>";
        else {
            switch ($message) {
                case "privilege":
                    $html = "Vous n'avez pas les droits pour executer cette action.";
                    break;
            } $_SESSION["notification"] = "<span style='color:red'>" . $html . "</span>";
        }
    }

    public function add_User() {
        if (User::getOne($_SESSION["id_booster"])->id_job > 2) {
            $user = new User();
            $user->id_booster = $_POST["id_booster"];
            $user->id_job = $_POST["id_job"];
            $user->id_manager = $_POST["id_manager"];
            $user->email = $_POST["id_booster"] . "@yopmail.com";
            $newPasswd = str_shuffle('abcdefghijklmnopqrstuvwxyz0123456789');
            $newPasswd = substr($newPasswd, 0, 8);
            $user->password = md5($newPasswd);
            $user->save("users");
            foreach ($_POST["campus"] as $campus) {
                $user_campus = new User_Campus();
                $user_campus->id_user = $_POST["id_booster"];
                $user_campus->id_campus = $campus;
                $user_campus->save();
            } self::notification("L'utilisateur a bien été enregistré.", "green");
            $message = "Votre compte vient d'être crée sur la plateforme SUPINFO Congé.\n";
            $message .= "Vous pouvez vous y connecter via l'adresse suivante : " . ROOT_URL . "\n";
            $message .= "Votre identifiant : " . $_POST["id_booster"] . "\n";
            $message .= "Votre mot de passe : " . $newPasswd . "\n";
            $message .= "Vous aurez toujours la possibilité de le modifier par la suite.\n";
            mail($user->email, "Supinfo-congé : Votre compte vient d'être crée", $message);
        } else
            self::notification("privilege");
        header("Location: " . ROOT_URL . "user/getall");
        exit();
    }

    public function update_Self_User() {
        $user = User::getOne($_SESSION["id_booster"]);
        if (($user->password === md5($_POST["password"])) && ($_POST["password1"] === $_POST["password2"])) {
            $user->firstname = $_POST["firstname"];
            $user->lastname = $_POST["lastname"];
            $_SESSION['fullname'] = $user->firstname . " " . $user->lastname;
            $user->password = md5($_POST["password1"]);
            $user->update();
            self::notification("Votre compte a été mis a jour.", "green");
            header("Location: " . ROOT_URL . "user/me");
        } else {
            self::notification("Les mots de passe ne correspondent pas.", "red");
            header("Location: " . ROOT_URL . "user/update_self");
        } exit();
    }

    public function update_User() {
        if (User::getOne($_SESSION["id_booster"])->id_job > 2) {
            $user = new User();
            $user->firstname = $_POST["firstname"];
            $user->lastname = $_POST["lastname"];
            $user->id_booster = $_POST["id_booster"];
            $user->id_job = $_POST["id_job"];
            $user->id_manager = $_POST["id_manager"];
            $user->password = $_POST["password"];
            $user->email = $_POST["id_booster"] . "@supinfo.com";
            $user->update("users");
            foreach ($_POST["campus"] as $campus) {
                $user_campus = new User_Campus();
                $user_campus->id_user = $_POST["id_booster"];
                $user_campus->id_campus = $campus;
                $user_campus->save();
            } self::notification("le compte a été mis a jour.", "green");
        } else
            self::notification("privilege");
        header("Location: " . ROOT_URL . "user/getall");
        exit();
    }

    public function delete_User() {
        $user = User::getOne($_GET["id"]);
        if ($user->id_manager == $_SESSION["id_booster"]) {
            $user->delete();
            self::notification("Le compte " . $user->id_booster . " à bien été supprimé.", "green");
            $message = "Votre compte vient d'être supprimé de la plateforme SUPINFO Congé";
            mail($user->email, "Supinfo-congé : Suppression de compte", $message);
        } else
            self::notification("privilege");
        header("Location: " . ROOT_URL . "user/getall");
        exit();
    }

    public function add_Leave() {

        $user = User::getOne($_SESSION["id_booster"]);
        $days = date_Compare(dateFormat($_POST["start"]), dateFormat($_POST["end"]))["days"];
        if (($user->getSolde() - $days) >= 0) {
            $leave = new Leave();
            $leave->id_booster = $user->id_booster;
            $leave->id_manager = $user->id_manager;
            $leave->start = dateFormat($_POST["start"]);
            $stopDate = new DateTime();
            $stopDate->setTime(00, 00, 00);
            //$stopDate->modify('+1 day');
            $date = new DateTime($leave->start);
            $leave->end = dateFormat($_POST["end"]);
            $leave->last_update = date('m/d/Y h:i:s a', time());
            if(isset($_POST["unpaid_leave"]) && $_POST["unpaid_leave"] == 'on') $leave->unpaid_leave = 1;
            if ($stopDate->getTimestamp() < $date->getTimestamp()) {
                $leave->save();
                self::notification("Votre demande de congé a bien été reçue, Vous recevrez un message lors de son traitement.", "green");
                $manager = User::getOne($user->id_manager);
                $message = "L'utilisateur " . $user->getFullname() . " (" . $user->id_booster . ") vient de soumettre une requete.\n";
                $message .= "Pensez à vous connecter a la platforme SUPINFO Congé pour traiter les requetes qui vous sont soumises.";
                mail($manager->email, "Supinfo-congé : Requete en attente traiter", $message);
            } else
                self::notification("Vous ne pouvez pas prendre de congés pour le jour même.", "red");
        } else
            self::notification("Vous n'avez pas assez de jour de congé en reserve pour effectuer cette action.", "red");
        header("Location: " . ROOT_URL . "leave/my");
        exit();
    }

    public function leave_Treatment() {
        $me = User::getOne($_SESSION["id_booster"]);
        foreach ($_POST as $requestId => $validation) {
            $request = Leave::getOne($requestId);
            if ($request->id_manager == $me->id_booster) {
                $requestUser = User::getOne($request->id_booster);
                if ($validation == 1) {
                    if ($me->id_job == 4) $request->status = 1;
                    else $request->id_manager = $me->id_manager;
                    $ltd = new Leave_Treatment_Date();
                    $ltd->id_manager = $me->id_booster;
                    $ltd->id_leave = $requestId;
                    $ltd->save();
                    $request->last_update = date("Y-m-d H:i:s");
                    $request->update();
                    $manager = User::getOne($me->id_manager);
                    if ($request->status == 1) {
                        $message = "Votre demande de congé du " . $request->start . " au " . $request->end . " à été validé.\n";
                        mail($requestUser->email, "Supinfo-congé : congé du " . $request->start . " au " . $request->end . " : validé", $message);
                    } else {
                        $message = "L'utilisateur " . $requestUser->getFullname() . "(" . $requestUser->id_booster . ") vient de soumettre une requete.\n";
                        $message .= "Pensez à vous connecter a la platforme SUPINFO Congé pour traiter les requetes qui vous sont soumises.";
                        mail($manager->email, "Supinfo-congé : Requete en attente traiter", $message);
                    }
                } else if ($validation == 0)
                    $request->delete();
                $message = "Votre demande de congé du " . $request->start . " au " . $request->end . " a été rejeté.\n";
                mail($requestUser->email, "Supinfo-congé : congé du " . $request->start . " au " . $request->end . " : Rejeté", $message);
            } else
                self::notification("privilege");
        } header("location: " . ROOT_URL . "leave/getall");
    }

    public function deleteMyLeave() {
        $me = User::getOne($_SESSION["id_booster"]);
        $leave = Leave::getOne($_GET["id"]);
        if ($leave->id_booster == $me->id_booster) {
            $leave->delete();
            self::notification("Votre demande à bien été supprimée", "green");
        } else
            self::notification("privilege");
        header("location: " . ROOT_URL . "leave/my");
    }

    public function add_Recovery() {
        $me = User::getOne($_SESSION["id_booster"]);
        $Recovery = new Recovery();
        $Recovery->id_booster = $me->id_booster;
        $Recovery->id_manager = $me->id_manager;
        $Recovery->day = dateFormat($_POST["day"]);
        $stopDate = new DateTime();
        $stopDate->setTime(00, 00, 00);
        //$stopDate->modify('+1 day');
        $date = new DateTime($Recovery->day);
        $Recovery->id_event = $_POST["id_event"];
        $Recovery->last_update = $date->format('Y-m-d');
        if ($stopDate->getTimestamp() < $date->getTimestamp()) {
            self::notification("Votre demande de récupération à bien été reçue, Vous recevrez un message lors de son traitement.", "green");
            $Recovery->save();
            $manager = User::getOne($me->id_manager);
            $message = "L'utilisateur " . $me->getFullname() . "(" . $me->id_booster . ") vient de soumettre une requete.\n";
            $message .= "Pensez à vous connecter a la platforme SUPINFO Congé pour traiter les requetes qui vous sont soumises.";
            mail($manager->email, "Supinfo-congé : Requete en attente traiter", $message);
        } else
            self::notification("Vous ne pouvez pas prendre de récup pour le jour même.", "red");
        header("location: " . ROOT_URL . "leave/my");
        exit();
    }

    public function recovery_Treatment() {
        $me = User::getOne($_SESSION["id_booster"]);
        foreach ($_POST as $requestId => $validation) {
            $request = Recovery::getOne($requestId);
            if ($request->id_manager = $_SESSION["id_booster"]) {
                $userRequest = User::getOne($request->id_booster);
                if ($validation == 1) { //Validation de la requete
                    if ($me->id_manager == NULL)
                        $request->status = 1;
                    else
                        $request->id_manager = $me->id_manager;
                    $rtd = new Recovery_Treatment_Date();
                    $rtd->id_manager = $me->id_booster;
                    $rtd->id_recovery = $requestId;
                    $rtd->save();
                    $request->last_update = date("Y-m-d H:i:s");
                    $request->update();
                    $manager = User::getOne($me->id_manager);
                    $requestUser = User::getOne($request->id_booster);
                    if ($request->status == 1) {
                        $message = "Votre demande de récuperation pour le " . $request->day . " à été validé.\n";
                        mail($requestUser->email, "Supinfo-congé : Récuperation du " . $request->day . " validé", $message);
                    } else {
                        $message = "L'utilisateur " . $requestUser->getFullname() . "(" . $requestUser->id_booster . ") vient de soumettre une requete.\n";
                        $message .= "Pensez à vous connecter a la platforme SUPINFO Congé pour traiter les requetes qui vous sont soumises.";
                        mail($manager->email, "Supinfo-congé : Requete en attente traiter", $message);
                    }
                } else if ($validation == 0)
                    $request->delete();
                $message = "Votre demande de récuperation (" . $request->day . ") à été rejeté.\n";
                mail($userRequest->email, "Supinfo-congé : Votre demande à été rejeté.", $message);
            } else
                self::notification("privilege");
        } header("location: " . ROOT_URL . "leave/getall");
    }

    public function deleteMyRecovery() {
        $me = User::getOne($_SESSION["id_booster"]);
        $recovery = Recovery::getOne($_GET["id"]);
        if ($recovery->id_booster == $me->id_booster) {
            $recovery->delete();
            self::notification("Votre demande à bien été supprimée", "green");
        } else
            self::notification("privilege");
        header("location: " . ROOT_URL . "leave/my");
    }

    public function subscribe() {
        $date = new DateTime();
        $user = User::getOne($_SESSION["id_booster"]);
        $SPR_Event = new Subscribe_Spr_Event ();
        $SPR_Event->id_booster = $user->id_booster;
        $SPR_Event->id_manager = $user->id_manager;
        $SPR_Event->id_event = $_POST["id_event"];
        $SPR_Event->last_update = $date->format('Y-m-d');
        $SPR_Event->save();
        self::notification("Votre inscription à bien été reçue, Vous recevrez un message lors de son traitement.", "green");
        $manager = User::getOne($user->id_manager);
        $message = "L'utilisateur " . $user->getFullname() . "(" . $user->id_booster . ") vient de soumettre une requete.\n";
        $message .= "Pensez à vous connecter a la platforme SUPINFO Congé pour traiter les requetes qui vous sont soumises.";
        mail($manager->email, "Supinfo-congé : Requete en attente traiter", $message);
        header("location: " . ROOT_URL . "leave/my");
        exit();
    }

    public function spr_Treatment() {
        foreach ($_POST as $requestId => $status) {
            $request = Subscribe_Spr_Event::getOne($requestId);
            if ($request->id_manager == $_SESSION["id_booster"]) {
                $me = User::getOne($_SESSION["id_booster"]);
                if ($status == 1) {
                    echo "Validé";
                    $request->status = 1;
                    $request->update();
                    $message = "Votre participation à l'évenement(" . $SPR_Event->id_event . ") à été validé.\n";
                } else {
                    $request->delete();
                    $message = "Votre inscription à l'évenement(" . $SPR_Event->id_event . ") à été rejeté.\n";
                } mail($userRequest->email, "Supinfo-congé : Votre demande à été rejeté.", $message);
            } else
                self::notification("privilege");
        } header("location: " . ROOT_URL . "leave/getall");
    }

    public function deleteMySubscribe() {
        $me = User::getOne($_SESSION["id_booster"]);
        $subscribe = Subscribe_Spr_Event::getOne($_GET["id"]);
        if ($subscribe->id_booster == $me->id_booster) {
            $subscribe->delete();
            self::notification("Votre demande à bien été supprimée", "green");
        } else
            self::notification("privilege");
        header("location: " . ROOT_URL . "leave/my");
    }

    public function add_Event() {
        $user = User::getOne($_SESSION["id_booster"]);
        if ($user->id_job >= 3) {
            $event = new SPR_Event();
            $event->id_category = $_POST["category"];
            $event->day = dateFormat($_POST["day"]);
            $event->description = $_POST["description"];
            $event->save();
            $currentEventID = SPR_Event::last_Entry();
            foreach ($_POST["campus"] as $id_campus) {
                $campus_event = new Campus_Event();
                $campus_event->id_campus = $id_campus;
                $campus_event->id_event = $currentEventID;
                $campus_event->save();
            } self::notification("L'événement a bien été ajouté.", "green");
        } else
            self::notification("privilege");
        header("Location: " . ROOT_URL . "leave/Event");
        exit();
    }

    public function reset_Passord() {
        $user = User::getOneWhere(["email" => $_POST["email"]]);
        if (!empty($user)) {
            $char = 'abcdefghijklmnopqrstuvwxyz0123456789';
            $newPasswd = str_shuffle($char);
            $newPasswd = substr($newPasswd, 0, 8);
            $user->password = md5($newPasswd);
            $user->update();
            $message = "Vous avez demandé une réiitialisation de mot de passe.\n";
            $message .= "Voici votre nouveau mot de passe : '" . $newPasswd . "'.\n";
            $message .= "Vous aurez toujours la possibilité de le modifier par la suite.\n";
            mail($user->email, 'Supinfo-congé : Réinitialisation de mot de passe', $message);
            self::notification("Votre mot de passe a été réinitialisé et envoyé sur votre boite mail.", "green");
        } else
            self::notification("Aucune adresse ne correspond. Veuillez entrer une adresse valide", "red");
    }

    public function super_user() {
        session_unset();
        session_destroy();
        session_start();
        $me = User::getOne($_POST["id_booster"]);
        $_SESSION["id_booster"] = $me->id_booster;
        $_SESSION["fullname"] = $me->getFullname();
        header("Location: " . ROOT_URL . "user/me");
    }

}
