<?php

class DefaultController {

    // ?ctrl=default&view=index
    public function index() {
        $title = "Index";
        $myList = array("Jean", "Michel", "Jarre");
        require_once(ROOT . "/views/default/index.php");
    }

    // ?ctrl=default&view=about
    public function about() {
        require_once(ROOT . "/views/default/about.php");
    }

}