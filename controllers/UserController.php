<?php

class UserController {

    public function index() {
        exit("user index");
    }

    public function login() {
        require_once(ROOT . "/views/users/login.php");
    }

    public function lostpassword() {
        require_once(ROOT . "/views/users/lostpassword.php");
    }

    public function me(){
        $me = User::getOne($_SESSION["id_booster"]);
        $me->job = $me->getJob();
        $me->campus = $me->getCampus();
        $recovery_day = count(Subscribe_Spr_Event::getAllWhere(["id_booster" => $me->id_booster,"status" => 1]));
        $event_Taken = count(Recovery::getAllWhere(["id_booster" => $me->id_booster]));
        $me->recovery_day = $recovery_day - $event_Taken;
        require_once(ROOT . "/views/users/me.php");
    }

    public function profil() {
        if(isset($_GET["id"]) && !empty($user)) $user = User::getOne($_GET["id"]);
        else $user = User::getOne($_SESSION["id_booster"]);
        $user->job = $user->getJob();
        $manager = User::getOne($user->id_manager);
        $user->campus = $user->getCampus();
        require_once(ROOT . "/views/users/profil.php");
    }

    public function update() {
        if(isset($_GET["id"])){
            $a_jobs = Job::getAll();
            $a_campus = Campus::getAll();
            $user = User::getOne($_GET["id"]);
            require_once(ROOT . "/views/users/update.php");
        } else exit("user update");
    }

    public function update_self(){
        $user = new User();
        $user = User::getOne($_SESSION["id_booster"]);
        require_once(ROOT . "/views/users/update_self.php");
    }

    public function getall() {
        $jobs = Job::getAll();
        $campus = Campus::getAll();

        $me = User::getOne($_SESSION["id_booster"]);
        $myUserList = User::getAllWhere(["id_manager" => $me->id_booster]);

        $UserList = User::getAllWhere(["id_manager" => $me->id_booster]);
        $usersView = [];
        foreach ($UserList as $user) {
            $userView = [];
            $userView["fullname"] = $user->getFullname();
            $userView["id_booster"] = $user->id_booster;
            $userView["job"] = $user->getJob();;
            $manager = User::getOne($user->id_manager);
            if(empty($manager)) $manager = User::getOne($user->id_booster);
            $userView["manager"]["id_booster"] = $manager->id_booster;
            $userView["manager"]["fullname"] = $manager->getFullname();
            $userView["campus"] = $user->getCampus();
            $userView["email"] = $user->email;
            $userView["leaveCount"] = $user->getSolde();
            $userView["RecoveryCount"] = count(Subscribe_Spr_Event::getAllWhere(["id_booster" => $user->id_booster,"status" => 1]));
            array_push($usersView, $userView);
        }

        require_once(ROOT . "/views/users/getall.php");
    }
}
