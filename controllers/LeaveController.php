
<?php

function hierarchicalChain($id_booster=NULL){
    $hierarchicalChain = array();
    if($id_booster!=NULL){
        $user = User::getOne($id_booster);
        foreach(Job::getAll() as $job){
          if($job->id > 1) $hierarchicalChain[$job->job] = NULL;
        }
        while($user->id_manager != NULL){
            $user = User::getOneWhere(["id_booster" => $user->id_manager]);
            $hierarchicalChain[Job::getOne($user->id_job)->job] = $user->id_booster;
        }
    } else {
        foreach(Job::getAll() as $job){
            if($job->id >= User::getOne($_SESSION["id_booster"])->id_job)
                array_push($hierarchicalChain, $job->job);
            else array_unshift ( $hierarchicalChain, $job->job);
        }

    } return $hierarchicalChain;
}

class LeaveController {

    // ?ctrl=leave&view=index
    public function index() {
        exit("leave index");
    }

    // ?ctrl=leave&view=modify
    public function update() {
        if(isset($_GET["id"])){
            $leave = Leave::getOne($_GET["id"]);
            require_once(ROOT . "/views/leave/update.php");
        } else exit("leave update");
    }

    public function my(){

        function notIn($table,$col, $subTable){
            $lessMyEvents = array();
            foreach ($table as $line){
                if (!in_array($line->$col, $subTable))
                    array_push($lessMyEvents, $line);
            } return $lessMyEvents;
        }

        $me = User::getOne($_SESSION["id_booster"]);
        $allEvents = SPR_Event::getAll();

        $a_holidays = Holiday::getAll();
        $currentYearHolidays = Holiday::getHolidays(date("Y"));
        $nextYearHolidays = Holiday::getHolidays(date("Y", strtotime('+1 years')));

        echo "<script>var allEvents =".json_encode($allEvents)."</script>";
        $hierarchicalChain = hierarchicalChain($_SESSION["id_booster"]);
        $jobs = Job::getAll();
        $category = Category::getAll();

        $myLeaveRequestView = [];
        $myLeavesRequests = Leave::getAllWhere(["id_booster" => $me->id_booster, "status" => 0]);
        foreach($myLeavesRequests as $leaveRequest){
            $RequestView = [];
            $RequestView["start"] = $leaveRequest->start;
            $RequestView["end"] = $leaveRequest->end;
            foreach ($hierarchicalChain as $job => $id_user){
              if($id_user!=NULL){
                $liaison = Leave_Treatment_Date::getOneWhere(["id_manager" => $id_user,"id_leave" => $leaveRequest->id]);
                $manager = User::getOne($id_user);
                $RequestView[$job]["id"] = $manager->id_booster;
                $RequestView[$job]["fullname"] = $manager->getFullname();
                if($liaison != NULL) $RequestView[$job]["date"] = $liaison->date;
                else $RequestView[$job]["date"] = NULL;
              }
            }
            $RequestView["submit_date"] = $leaveRequest->submit_date;
            $RequestView["delete"] = $leaveRequest->id;
            array_push($myLeaveRequestView, $RequestView);
        }

        $myLeaveView = [];
        $myLeaves = Leave::getAllWhere(["id_booster" => $me->id_booster, "status" => 1]);
        foreach($myLeaves as $leave){
            $view = [];
            $view["start"] = $leave->start;
            $view["end"] = $leave->end;
            foreach ($hierarchicalChain as $job => $id_user){
              if($id_user!=NULL){
                $liaison = Leave_Treatment_Date::getOneWhere(["id_manager" => $id_user,"id_leave" => $leave->id]);
                $manager = User::getOne($id_user);
                $view[$job]["id"] = $manager->id_booster;
                $view[$job]["fullname"] = $manager->getFullname();
                if($liaison != NULL) $view[$job]["date"] = $liaison->date;
                else $view[$job]["date"] = NULL;
              }
            }
            $view["submit_date"] = $leave->submit_date;
            array_push($myLeaveView, $view);
        }

        $myRecoveryRequests = Recovery::getAllWhere(["id_booster" => $me->id_booster, "status" => 0]);
        $myRecoveryRequestView = [];
        foreach($myRecoveryRequests as $recoveryRequest){
            $RequestView = [];
            $RequestView["start"] = $recoveryRequest->day;
            $RequestView["event"] = json_encode(SPR_Event::getOne($recoveryRequest->id_event));
            foreach ($hierarchicalChain as $job => $id_user){
              if($id_user!=NULL){
                $liaison = Recovery_Treatment_Date::getOneWhere(["id_manager" => $id_user,"id_recovery" => $recoveryRequest->id]);
                $manager = User::getOne($id_user);
                $RequestView[$job]["id"] = $manager->id_booster;
                $RequestView[$job]["fullname"] = $manager->getFullname();
                if($liaison != NULL) $RequestView[$job]["date"] = $liaison->date;
                else $RequestView[$job]["date"] = NULL;
              }
            }
            $RequestView["submit_date"] = $recoveryRequest->submit_date;
            $RequestView["delete"] = $recoveryRequest->id;
            array_push($myRecoveryRequestView, $RequestView);
        }

        $myRecoverys = Recovery::getAllWhere(["id_booster" => $me->id_booster, "status" => 1]);
        $myRecoveryView = [];
        foreach($myRecoverys as $recovery){
            $event = SPR_Event::getOne($recovery->id_event);
            $view = [];
            $view["date"] = $recovery->day;
            $view["event"] = json_encode($event);
            foreach ($hierarchicalChain as $job => $id_user){
              if($id_user!=NULL){
                $liaison = Recovery_Treatment_Date::getOneWhere(["id_manager" => $id_user,"id_recovery" => $recovery->id]);
                $manager = User::getOne($id_user);
                $view[$job]["id"] = $manager->id_booster;
                $view[$job]["fullname"] = $manager->getFullname();
                if($liaison != NULL) $view[$job]["date"] = $liaison->date;
                else $view[$job]["date"] = NULL;
              }
            }
            $view["submit_date"] = $recovery->submit_date;
            array_push($myRecoveryView, $view);
        }

        $mySubscribes = Subscribe_Spr_Event::getAllWhere(["id_booster" => $me->id_booster, "status" => 0]);
        $mySprEventRequestView = [];
        foreach ($mySubscribes as $registration) {
            $mySubscribeEvent = SPR_Event::getOne($registration->id_event);
            if(!empty($mySubscribeEvent)){
                $RequestView = [];
                $RequestView["id"] = $registration->id;
                $RequestView["category"] = $mySubscribeEvent->getCategory();
                $RequestView["date"] = $mySubscribeEvent->day;
                $RequestView["description"] = $mySubscribeEvent->description;
                $manager = User::getOne($me->id_manager);
                if(empty($manager)) $manager = User::getOne($me->id_booster);
                $RequestView["manager"]["id"] = $manager->id_booster;
                $RequestView["manager"]["fullname"] = $manager->getFullname();
                $RequestView["last_update"] = $registration->last_update;
                $RequestView["submit_date"] = $registration->submit_date;
                array_push($mySprEventRequestView, $RequestView);
            }
        }

        $myparticipations = Subscribe_Spr_Event::getAllWhere(["id_booster" => $me->id_booster, "status" => 1]);
        $myparticipationsView = [];
        foreach ($myparticipations as $participation) {
            $event = SPR_Event::getOne($participation->id_event);
            if(!empty($event)){
                $View = [];
                $View["id"] = $participation->id;
                $View["category"] = $event->getCategory();
                $View["date"] = $event->day;
                $View["description"] = $event->description;
                $manager = User::getOne($me->id_manager);
                if(empty($manager)) $manager = User::getOne($me->id_booster);
                $View["manager"]["id"] = $manager->id_booster;
                $View["manager"]["fullname"] = $manager->getFullname();
                $View["last_update"] = $participation->last_update;
                $View["submit_date"] = $participation->submit_date;
                array_push($myparticipationsView, $View);
            }
        }

        $allEvents = SPR_Event::getAll();
        $leave_treatment_date = Leave_Treatment_Date::getAll();

        $myLeaves = Leave::getAllWhere(["id_booster" => $me->id_booster]);
        $myLeaveList = Leave::getAllWhere(["id_booster" => $me->id_booster, "status" => 1]);

        $myRecovery = Recovery::getAllWhere(["id_booster" => $me->id_booster]);
        $myRecoveryList = Recovery::getAllWhere(["id_booster" => $me->id_booster, "status" => 1]);

        $myAllEvent = Subscribe_Spr_Event::getAllWhere(["id_booster" => $me->id_booster]);
        $mySprEventList = Subscribe_Spr_Event::getAllWhere(["id_booster" => $me->id_booster, "status" => 1]);

        $recoveryTaken = array();
        foreach ($myRecovery as $recovery){
            array_push($recoveryTaken, $recovery->id_event);
        } $eventsToRecover = notIn($mySprEventList,"id_event", $recoveryTaken);
        $enablesEventsView = [];
        foreach ($eventsToRecover as $eventToRecover) {
            $event = SPR_Event::getOne($eventToRecover->id_event);
            $enableEventView = [];
            $enableEventView["id_event"] = $eventToRecover->id_event;
            $enableEventView["category"] = $event->getCategory();
            $enableEventView["day"] = $event->day;
            $enableEventView["description"] = $event->description;
            array_push($enablesEventsView, $enableEventView);
        }


        $subscribedEvent = array();
        foreach ($myAllEvent as $event){
            array_push($subscribedEvent, $event->id_event);
        }

        $eventJob = Spr_Event::getAllWhere(["id_category" => 1]);
        $eventJob = notIn($eventJob, "id", $subscribedEvent);
        $eventJpo = Spr_Event::getAllWhere(["id_category" => 2]);
        $eventJpo = notIn($eventJpo, "id", $subscribedEvent);
        $eventOther = Spr_Event::getAllWhere(["id_category" => 3]);
        $eventOther = notIn($eventOther, "id", $subscribedEvent);

        $enableEvents = [];
        foreach(User_Campus::getAllWhere(["id_user" => $me->id_booster]) as $user_campus){
            foreach(Campus_event::getAllWhere(["id_campus" => $user_campus->id_campus]) as $campus_event){
                $event = SPR_Event::getOne($campus_event->id_event);
                if(!in_array($event, $enableEvents)) array_push($enableEvents, $event);
            }
        }

        $a_events_category = [];
        foreach(Category::getAll() as $category){
            $a_events = [];
            foreach($enableEvents as $event){
                if($category->id == $event->id_category) array_push($a_events, $event);
                $a_events_category[$category->id] = $a_events;
            }
        }
        $a_category = Category::getAll();

        require_once(ROOT . "/views/leave/my.php");
    }

    public function getall() {
        $me = User::getOne($_SESSION["id_booster"]);

        $jobs = Job::getAll();
        $category = Category::getAll();
        $hierarchicalChain;
        $leaveRequestsView = [];
        $leaveRequests = Leave::getAllWhere(["id_manager" => $me->id_booster, "status" => 0]);
        if(!empty($leaveRequests)){
            foreach($leaveRequests as $leaveRequest){
                $user = User::getOne($leaveRequest->id_booster);
                $leaveHierarchicalChain = hierarchicalChain($user->id_booster);
                $requestView = [];
                $requestView["id"] = $leaveRequest->id;
                $requestView["id_booster"] = $leaveRequest->id_booster;
                $requestView["fullname"] = $user->getFullname();
                $requestView["start"] = $leaveRequest->start;
                $requestView["end"] = $leaveRequest->end;
                $requestView["leave_point"] = $leaveRequest->getDuration();
                foreach ($leaveHierarchicalChain as $job => $id_user){
                  if($id_user!=NULL){
                    $liaison = Leave_Treatment_Date::getOneWhere(["id_manager" => $id_user,"id_leave" => $leaveRequest->id]);
                    $manager = User::getOne($id_user);
                    $requestView[$job]["id"] = $manager->id_booster;
                    $requestView[$job]["fullname"] = $manager->getFullname();
                    if($liaison != NULL) $requestView[$job]["date"] = $liaison->date;
                    else $requestView[$job]["date"] = NULL;
                  } else {
                    $requestView[$job]["id"] = NULL;
                    $requestView[$job]["fullname"] = NULL;
                    $requestView[$job]["date"] = NULL;
                  }
                }
                $requestView["submit_date"] = $leaveRequest->submit_date;
                array_push($leaveRequestsView, $requestView);
            }
        } else $leaveHierarchicalChain = hierarchicalChain();


        $recoveryRequestsView = [];
        $recoveryRequests = Recovery::getAllWhere(["id_manager" => $me->id_booster, "status" => 0]);
        if(!empty($recoveryRequests)){
            foreach($recoveryRequests as $recoveryRequest){
                $user = User::getOne($recoveryRequest->id_booster);
                $recoveryHierarchicalChain = hierarchicalChain($user->id_booster);
                $requestView = [];
                $requestView["id"] = $recoveryRequest->id;
                $requestView["id_booster"] = $recoveryRequest->id_booster;
                $requestView["fullname"] = $user->getFullname();
                $requestView["day"] = $recoveryRequest->day;
                $requestView["id_event"] = $recoveryRequest->id_event;
                foreach ($recoveryHierarchicalChain as $job => $id_user){
                  if($id_user!=NULL){
                    $liaison = Recovery_Treatment_Date::getOneWhere(["id_manager" => $id_user,"id_recovery" => $recoveryRequest->id]);
                    $manager = User::getOne($id_user);
                    $requestView[$job]["id"] = $manager->id_booster;
                    $requestView[$job]["fullname"] = $manager->getFullname();
                    if($liaison != NULL) $requestView[$job]["date"] = $liaison->date;
                    else $requestView[$job]["date"] = NULL;
                  } else {
                    $requestView[$job]["id"] = NULL;
                    $requestView[$job]["fullname"] = NULL;
                    $requestView[$job]["date"] = NULL;
                  }
                }
                $requestView["submit_date"] = $recoveryRequest->submit_date;
                array_push($recoveryRequestsView, $requestView);
            }
        } else $recoveryHierarchicalChain = hierarchicalChain();

        $subscribes = Subscribe_Spr_Event::getAllWhere(["id_manager" => $me->id_booster, "status" => 0]);
        $sprEventRequestView = [];
        foreach ($subscribes as $registration) {
            $user = User::getOne($registration->id_booster);
            $subscribeEvent = SPR_Event::getOne($registration->id_event);
            $requestView = [];
            $requestView["id"] = $registration->id;
            $requestView["id_booster"] = $registration->id_booster;
            $requestView["fullname"] = $user->getFullname();
            $requestView["day"] = $subscribeEvent->day;
            $requestView["id_event"] = $registration->id_event;
            $manager = User::getOne($me->id_manager);
            if(empty($manager)) $manager = User::getOne($me->id_booster);
            $requestView["manager"]["id"] = $manager->id_booster;
            $requestView["manager"]["fullname"] = $manager->getFullname();
            $requestView["last_update"] = $registration->last_update;
            $requestView["submit_date"] = $registration->submit_date;
            array_push($sprEventRequestView, $requestView);
        }

        require_once(ROOT . "/views/leave/getall.php");
    }

     public function calendar(){
        $currentYearHolidays = Holiday::getHolidays(date("Y"));
        $nextYearHolidays = Holiday::getHolidays(date("Y", strtotime('+1 years')));

        $allUsers = User::getAll();
        $LeaveList = Leave::getAll();
        $RecoveryList = Recovery::getAll();
        $allEvent = SPR_Event::getAll();

        $aManager = User::getAllWhere(["id_job"=>'1']);
        $cManager = User::getAllWhere(["id_job"=>'2']);
        $aRegion = User::getAllWhere(["id_job"=>'3']);
        $pRegion = User::getAllWhere(["id_job"=>'4']);
        $campus = Campus::getAll();

        $campusNames = array();
        $campusLeaves = array();
        $leave_campus = array();
        foreach ($campus as $element){
            array_push($campusNames,$element->name);
        }

        $a_campusUsers = [];
        foreach($campus as $campusS){
            $a_userCampus = [];
            foreach(User_Campus::getAllWhere(["id_campus" => $campusS->id]) as $userCampus){
                foreach(Leave::getAllWhere(["id_booster" => $userCampus->id_user]) as $leave){
                    if(!in_array($leave,$a_campusUsers)){
                        array_push($a_userCampus, $leave);
                    }
                }
            } array_push($a_campusUsers, $a_userCampus);
        }
        require_once(ROOT . "/views/leave/calendar.php");
    }

    public function event(){
        $me = User::getOne($_SESSION["id_booster"]);
        $category = Category::getAll();
        $region = Region::getAll();
        $campus = Campus::getAll();
        $events = SPR_Event::getAll();
        $allEvents = [];
        foreach($events as $event){
            $allCampusEvent="";
            foreach(Campus_Event::getAllWhere(["id_event" => $event->id]) as $campus_event){
                $allCampusEvent .= Campus::getOne($campus_event->id_campus)->name.", ";
            } $allCampusEvent = rtrim($allCampusEvent, ", ").".";
        $allEvents[$event->id] = $allCampusEvent;
        }
        require_once(ROOT."views/leave/event.php");
    }
}
